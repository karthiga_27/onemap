<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$pagename = "City";
	$redirecturl = "city.php";
	$currenturl = "city-view.php";
	
	$message="";
	
	if(isset($_GET['id']) && $_GET['id'] != "") {
		$query = "delete from fyc_city where (cityid = ".$_GET['id'].")";
		$equery = mysql_query($query) or die(mysql_error());
		$message = "Record deleted successfully";
	}
	elseif(isset($_GET['mode']) && $_GET['mode'] != "") {
		$message = "Record ".$_GET['mode']."d successfully";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include('includes/admin-meta.php') ?>
<script language="JavaScript" type="text/javascript">
	function onchangecountry(countrycontrol) {
		window.location.href = "<?php echo $currenturl; ?>?countryid="+countrycontrol.value;
	}
</script>
</head>

<body>

<!-- body Begin -->
  <div id="wrapper">
         <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            
          <div class="container">
            <h4><?php echo $pagename; ?></h4></div>

<?php 
	if(isset($_GET['countryid']) && $_GET['countryid'] != "") { $countryurl = "?countryid=".$_GET['countryid']; }
	    echo "<div class='row'><div class='col-md-10 hidden-sm hidden-xs'>";
        echo "<a href='$redirecturl' class='btn btn-info' role='button' title='Click here to add new $pagename'style='float:right'>Add New</a>";
        echo "</div></div>";
?>

    <div class="container"> 
    	<div class="col-lg-6">
    	<div class="form-group">
			    <label for="Country">Country</label>
			<select id="country" name="country" class="form-control" onchange="onchangecountry(this)">
				<option value="">--- Select Country ---</option>
				<?php
					$countryid = $_GET['countryid'];
					$query = "select countryid, country from fyc_country order by country asc";
					$equery = mysql_query($query) or die(mysql_error());
					while($fetchrow = mysql_fetch_array($equery)) {
						if($fetchrow['countryid'] == $countryid) {
							echo "<option selected value='".$fetchrow['countryid']."'>".$fetchrow['country']."</option>";
						}
						else {
							echo "<option value='".$fetchrow['countryid']."'>".$fetchrow['country']."</option>";
						}
					}
				?>
			</select>
		</div>
    </div></div>
	
	<?php
	 $start=0;
	 if(isset($_GET['forward']))
	 {
		 $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
		 $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
		 
		 if($_GET['forward']>=1)
		 {
			 $start=$_GET['forward'];
		 }
		 else
		 {
			 $start=0;
		 }
	 }
	    $wherecondition="";
		if($countryid != "") { $wherecondition = " where (fyc_city.countryid = '".$countryid."') "; }
		$query = "select fyc_city.cityid, fyc_city.city, fyc_city.countryid, fyc_country.country, fyc_city.citystatus 
			from fyc_city left outer join fyc_country on fyc_city.countryid = fyc_country.countryid 
			$wherecondition order by fyc_city.city asc limit $start ,11";
		$equery = mysql_query($query);
	
		if($message != "") {
			echo "<tr><td colspan='8'><b><font color='green'>".$message."</font></b></td></tr>";
		}
		echo "<table><thead>";
		echo "<thead><tr><th>Sl No</th><th>".$pagename."</th><th>Country</th><th>Status</th><th>Edit</th><th>Delete</th></tr></thead>";
		if(mysql_num_rows($equery) == 0) {
			echo "<tr><td colspan='8' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
		}
		else {
			$irow = 1;
			while($fetchrow = mysql_fetch_array($equery)) {
				//echo "<tr onmouseover=\"this.className='onmouseovertr';\" onclick=\"window.location.href='$redirecturl?id=$fetchrow[0]';\" onmouseout=\"this.className='onmouseouttr'\" title='Click here to edit this record'>";
				echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
				echo "<td data-column='Sl No'>".$fetchrow['cityid']."</td>";
				echo "<td data-column='$pagename'>".$fetchrow['city']."</td>";
				echo "<td data-column='Country'>".$fetchrow['country']."</td>";
				echo "<td data-column='Edit'>";
				if($fetchrow['citystatus'] == "1") { echo "Active"; } else { echo "InActive"; }
				echo"</td>";
				echo "<td data-column='Status'><a href='$redirecturl?cityid=".$fetchrow['cityid']."'><img src='images/edit.gif' alt='Edit' title='Edit'></a></td>";
				echo "<td data-column='Delete'><a href='#' onclick=\"DeleteConfirmation($fetchrow[cityid])\"><img src='images/remove.png' style='width: 25px;height: 25px; alt='Delete' title='Delete' /></a></td>";
				echo "</tr></tbody>";
				$irow += 1;
			}
		}
	?>
	</thead></table>
	<?php
      if($start>=10)
      {
          $previous=$start-10;
          if(isset($_GET['sid']))
          {
          echo "<a href='city-view.php?sid=$_GET[sid]&forward=$previous'><img style='width:30px;' src='images/previous.png' /></a>";
          }
          else
          {
          echo "<a href='city-view.php?forward=$previous'><img style='width:30px;' src='images/previous.png' /></a>";
          }
      }
      echo "&nbsp;&nbsp;&nbsp;";
      if(mysql_num_rows($equery)==11)
      {
          $next=$start+10;
          if(isset($_GET['sid']))
          {
          echo "<a href='city-view.php?sid=$_GET[sid]&forward=$next'><img style='width:30px;' src='images/forward.png' /></a>";
          }
          else
          {
          echo "<a href='city-view.php?forward=$next'><img style='width:30px;' src='images/forward.png' /></a>";
          }
      }
      ?>
 </div>
      </div>
 </div>
 <script>
     function DeleteConfirmation(id) {
         if(confirm('Are You Sure to Delete?'))
            window.location.href="city-view.php?id="+id; 
     }
 </script>
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>