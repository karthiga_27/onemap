<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	if($_SESSION[$logincategory]==1 && isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
	    {
	        $Query = "delete from fyc_advertisement where advertisementid = '$_GET[id]'";
		    $EQuery = mysql_query($Query) or die(mysql_error());
		    
			$message = "Record deleted successfully";
	    }
		else
		{
		    $errormessage="Invalid event";
		}
	}
	
	if(isset($_GET['message'])) 
	{
		$_GET['message']=mysql_escape_string(htmlentities($_GET['message'])); 
		$message = "Record ".$_GET['message']."d successfully";
	}
	else if(isset($_GET['errormessage'])) 
	{
		$_GET['errormessage']=mysql_escape_string(htmlentities($_GET['errormessage'])); 
		$errormessage=$_GET['errormessage'];
	}
	
?>

<!DOCTYPE HTML>
<html>
</head>
    <?PHP include('includes/admin-meta.php') ?>
</head>
<body>
  <div id="wrapper">
         <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container"> <div class="container">
        <h4>Advertisement</h4>
		<?php
		
		$start=0;
		if(isset($_GET['forward']))
		{
		    $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
		    $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
	        
			if($_GET['forward']>=1)
			{
			    $start=$_GET['forward'];
		    }
			else
			{
			    $start=0;
			}
		}
		
		$query = "select advertisementid, advertisement, advertisementstatus from fyc_advertisement order by advertisementid desc limit $start, 11 ";
	    $equery = mysql_query($query);
		
		echo "<div class='row'><div class='col-md-10 hidden-sm hidden-xs'>";
        echo "<a href='advertising.php' title='Click here to add new event' class='btn btn-info' role='button' style='float:right'>Add New</a>";
        echo "</div></div>";
		
		echo "<table>";
		
		if($message != "") 
	    {
		    echo "<tr><td><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		echo "<thead><tr><th>Sl No</th><th>Add</th><th>Status</th><th>E</th>";
		
		
		if($_SESSION[$logincategory]==1)
	    {
	        echo "<th>D</th></tr></thead>";
	    }
		
		if(mysql_num_rows($equery)==0) 
	    {
		    echo "<tr><td colspan='7' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
	    }
		else
		{
		    $iRow = 1;
			while( $iRow<=10 && $fetchrow = mysql_fetch_array($equery))
            {
                echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
			    echo "<td data-column='Sl No'>".$iRow."</td>";
			    echo "<td data-column='Add'>".$fetchrow['advertisement']."</td>";
				
				echo "<td data-column='Status'>";
			    if($fetchrow['Add'] == "1") { echo "Active"; } else { echo "InActive"; }
			    echo"</td>";

				echo "<td data-column='E'><a href='advertising.php?id=$fetchrow[advertisementid]&forward=$start'><img src='../images/edit.gif' alt='Edit' title='Edit'></a></td>";
            
			    if($_SESSION[$logincategory]==1)
	            {
			        echo "<td data-column='D'><a href='#' onclick=\"DeleteConfirmation('advertising-view.php?id=$fetchrow[advertisementid]&forward=$start','$fetchrow[advertisement]')\"><img src='../images/delete.jpg' alt='Delete' title='Delete' /></a></td>";
		        }
			
			    echo "</tr></tbody>";
			    $iRow += 1;
			}	
        }
		echo "</table>";
		
	
		
			if($start>=10)
			{
			$previous=$start-10;
			echo "<a href='advertising-view.php?forward=$previous'><img src='../images/previous.png' /></a>";
			}
			echo "&nbsp;&nbsp;&nbsp;";
			if(mysql_num_rows($equery)==11)
			{
			$next=$start+10;
			echo "<a href='advertising-view.php?forward=$next'><img src='../images/forward.png' /></a>";
			}
       		
		
		
		?></div>
        </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>
