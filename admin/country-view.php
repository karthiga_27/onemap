<?php
  include("includes/config-variables-admin.php");
  include("includes/config.php");
  if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
  
  $pagename = "Country";
  $redirecturl = "country.php";
  $currenturl = "country-view.php";
  $tablename = "fyc_country";
  $cityname="fyc_city";
  $idfield = "countryid";
  $idfields = "cityid";
  $namefield ="country";
  
  $statusfield = "countrystatus";
  
  $message="";
  if(isset($_GET['id'])) 
  {
    
    $Query = "delete from $cityname where ($idfield = '$_GET[id]')";
    // echo $_GET['id'];
    $EQuery = mysql_query($Query) or die(mysql_error());
    
    $Query = "delete from $tablename where ($idfield = '$_GET[id]')";
    $EQuery = mysql_query($Query) or die(mysql_error());
    
    $message = "Record deleted successfully";
  }
  elseif(isset($_GET['mode']) && $_GET['mode'] != "") 
  {
    $message = "Record ".$_GET['mode']."d successfully";
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include('includes/admin-meta.php') ?>
</head>

<body>

<!-- body Begin -->
  <div id="wrapper">
         <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
          <div class="container">
            <h4><?php echo $pagename; ?></h4></div>
                  <?PHP
                  $start=0;
                  if(isset($_GET['forward']))
                  {
                      $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
                      $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
                      
                      if($_GET['forward']>=1)
                      {
                          $start=$_GET['forward'];
                      }
                      else
                      {
                          $start=0;
                      }
                  }
                    $query = "select $idfield, $namefield, $statusfield from $tablename order by $namefield asc limit $start ,11";
                    $equery = mysql_query($query);
                    echo "<div class='row'><div class='col-md-10 hidden-sm hidden-xs'>";
                    echo "<a href='$redirecturl' class='btn btn-info' role='button' title='Click here to add new $pagename'style='float:right'>Add New</a>";
                    echo "</div></div>";
                    echo "<table><thead>";
                    if($message != "") {
                      echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
                    }
                    echo "<thead><tr><th>Sl No</th><th>".$pagename."</th><th>Status</th><th>Edit</th><th>Delete</th></tr></thead>";
                    if(mysql_num_rows($equery) == 0) {
                      echo "<tr><td colspan='7' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
                    }
                    else {
                      $iRow = 1;
                      while($fetchrow = mysql_fetch_row($equery)) {
                        //echo "<tr onmouseover=\"this.className='onmouseovertr';\" onclick=\"window.location.href='$redirecturl?id=$fetchrow[0]';\" onmouseout=\"this.className='onmouseouttr'\" title='Click here to edit this record'>";
                        echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
                        echo "<td data-column='Sl No'>".$iRow."</td>";
                        echo "<td data-column='$pagename'>".$fetchrow[1]."</td>";
                        echo "<td data-column='Status'>";
                        if($fetchrow[2] == "1") { echo "Active"; } else { echo "InActive"; }
                        echo"</td>";
                        echo "<td data-column='Edit'><a href='$redirecturl?id=$fetchrow[0]'><img src='images/edit.gif' style='width: 25px;height: 25px; alt='Edit' title='Edit'></a></td>";
                        echo "<td data-column='Delete'><a href='#' onclick=\"DeleteConfirmation($fetchrow[0])\"><img src='images/remove.png' style='width: 30px;height: 30px;' alt='Delete' title='Delete' /></a></td>";
                        echo "</tr></tbody>";
                        $iRow += 1;
                      }
                    }
                    echo "</thead></table>";
                  ?>
<?php
      if($start>=10)
      {
          $previous=$start-10;
          if(isset($_GET['sid']))
          {
          echo "<a href='country-view.php?sid=$_GET[sid]&forward=$previous'><img style='width:30px;' src='images/previous.png' /></a>";
          }
          else
          {
          echo "<a href='country-view.php?forward=$previous'><img style='width:30px;' src='images/previous.png' /></a>";
          }
      }
      echo "&nbsp;&nbsp;&nbsp;";
      if(mysql_num_rows($equery)==11)
      {
          $next=$start+10;
          if(isset($_GET['sid']))
          {
          echo "<a href='country-view.php?sid=$_GET[sid]&forward=$next'><img style='width:30px;' src='images/forward.png' /></a>";
          }
          else
          {
          echo "<a href='country-view.php?forward=$next'><img style='width:30px;' src='images/forward.png' /></a>";
          }
      }
      ?>
 </div>
      </div>
 </div>
 <script>
     function DeleteConfirmation(id) {
         if(confirm('Are You Sure to Delete?'))
            window.location.href="country-view.php?id="+id; 
     }
 </script>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>