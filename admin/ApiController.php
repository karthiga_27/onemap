<?php
header("Access-Control-Allow-Origin: *");

if (isset($_SERVER['HTTP_ORIGIN']))
{

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); // cache for 1 day
    
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) 
    header("Access-Control-Allow-Methods: GET, POST,PUT ,DELETE OPTIONS");
    header("Access-Control-Allow-Headers: *");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) 
    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
class ApiController extends Controller
{

    // Members
    
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */

    const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */

    private $format = 'json';

    /**
     * @return array action filters
     */

    public function filters()
    {

        return array();

    }

    //// login
    

    public function actionlogin()
    {

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $role = $_POST['role'];
        $model = FycMember::model();

        $cri = new CDbCriteria();

        $cri->select = 'memberid, firstname, memberstatus as status';
        $cri->condition = "email = '" . $email . "' and password = '" . $password . "'";
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($users) <= 0)
        {
            $this->_sendResponse(504, $users);
        }
        else
        {
            $model = FycMember::model()->findByAttributes(array("email"=>$email, "password"=>$password));
            
            if($model->memberstatus == $role)
               $this->_sendResponse(200, $users);
            else
               $this->_sendResponse(505, $users);
        }

    }

    public function actiontestindiatime()
    {
        date_default_timezone_set('Asia/Kolkata');
        echo date('d-m-Y H:i:s');
    }

    ////     Register
    public function actionregister()
    {

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }

        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $telephone = isset($_POST['phone']) ? $_POST['phone'] : "";
        $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : "";
        $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : "";
        //$bid = isset($_POST['bid']) ? $_POST['bid'] : 0;
        $role = isset($_POST['role']) ? $_POST['role'] : "";

        $model = new FycMember();
        // $now = new DateTime();
        $model->password = md5($_POST['password']);
        $model->email = $email;
        $model->telephone = $telephone;
        $model->firstname = $firstname;
        $model->lastname = $lastname;
        $model->memberstatus = $role;
//        if ($bid != 1) $model->memberstatus = '1';
//        else $model->memberstatus = '2';

        if ($model->save())
        {
            $cri = new CDbCriteria();

            $cri->select = 'memberid, memberstatus as status';

            $cri->condition = "memberid = " . $model->memberid;

            $users = $model->getCommandBuilder()
                ->createFindCommand($model->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }

        else
        {
            $this->_sendResponse(500, array());

        }

    }

    //    ////     Register
    //
    //    public function actionregister() {
    //
    //        if (json_decode(file_get_contents("php://input"), true)) {
    //
    //            $_POST = json_decode(file_get_contents("php://input"), true);
    //
    //        }
    //
    //
    //
    //        $rid = $_POST['rid'];
    //        $bid = $_POST['bid'];
    //        $email = $_POST['email'];
    //        $telephone = $_POST['phone'];
    //        $firstname = $_POST['firstname'];
    //        // $bid = $_POST['bid'];
    //
    //
    //        $model = new FycMember();
    //        $now = new DateTime();
    //
    //        $model->password = md5($_POST['password']);
    //        $model->email = $email;
    //        $model->telephone = $telephone;
    //        $model->firstname = $firstname;
    //
    //
    //        if(($rid)=='1'){
    //        $model->memberstatus = '1';
    //        }
    //        if(($bid)=='1'){
    //            $model->memberstatus = '2';
    //            }
    //
    //
    //        if ($model->save()) {
    //            $cri = new CDbCriteria();
    //
    //            $cri->select = 'memberid';
    //
    //            $cri->condition = "memberid = " . $model->memberid;
    //
    //            $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
    //
    //            $this->_sendResponse(200, $users);
    //
    //        }
    //
    //
    //
    //    else {
    //            $this->_sendResponse(500, array());
    //
    //        }
    //
    //    }
    // Get category
    public function actioncategory()
    {

        $this->_authenticate();

        $model = FycCategory::model();

        $cri = new CDbCriteria();

        $cri->select = "categoryid, category,icon, categorystatus";

        $cri->condition = "categorystatus ='1'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }
    
    public function actioncardcategory()
    {

        $this->_authenticate();
        
        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycCardCategories::model();

        $cri = new CDbCriteria();

        $cri->select = "id, name,memberid, created";

        $cri->condition = "memberid ='$id'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    // Get categoryname
    public function actioncategoryname()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycCategory::model();

        $cri = new CDbCriteria();

        $cri->select = " category";

        $cri->condition = "categoryid =$id";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    //// Get subcategory
    

    public function actiontestsubcategory()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycSubcategory::model();

        $cri = new CDbCriteria();

        $cri->select = "subcategoryid, subcategory, icon, keywords, description, title";
        $cri->condition = "categoryid =$id";
        $cri->order = "subcategory asc";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actionsubcategory()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycSubcategory::model();

        $cri = new CDbCriteria();
        $cri->alias = 'sub';
        $cri->select = "COUNT(dir.subcategoryid) as directorycount, sub.subcategoryid,sub.categoryid, sub.subcategory, sub.icon, sub.description, sub.title, sub.keywords";
        $cri->join = "left outer join fyc_directory as dir on sub.subcategoryid = dir.subcategoryid";
        $cri->condition = "sub.categoryid =$id";
        $cri->group = "sub.subcategoryid";
        $cri->order = "sub.subcategory asc";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }
    
     public function actiongetcountries()
     {
         $this->_authenticate();

         $body = array();
         
        $countriesarr = array();
        
        $ccri = new CDbCriteria();
        $ccri->select = 'id,name';
        $ccri->condition = "status = '1'";

        $countrymodel = FycCountries::model()->getCommandBuilder()->createFindCommand(FycCountries::model()->tableSchema, $ccri)->queryAll();

         if (!empty($countrymodel))
        {

            $this->_sendResponse(200, $countrymodel);

        }
        else
        {

            $this->_sendResponse(507, array());

        }
     }
     
     public function actiongetstate()
     {
         $this->_authenticate();
         
         $body = array();
         
         $id = $_GET['id'];
                 
        $ccri = new CDbCriteria();
        $ccri->select = 'id,name';
        $ccri->condition = "country_id = '$id'";

        $countrymodel = FycStates::model()->getCommandBuilder()->createFindCommand(FycStates::model()->tableSchema, $ccri)->queryAll();

        if (!empty($countrymodel))
        {

            $this->_sendResponse(200, $countrymodel);

        }
        else
        {

            $this->_sendResponse(507, array());

        }
     }
     
     public function actiongetcity()
     {
        $this->_authenticate();
         
        $body = array();
         
         $id = $_GET['id'];
                 
        $ccri = new CDbCriteria();
        $ccri->select = 'id,name';
        $ccri->condition = "state_id = '$id'";

        $countrymodel = FycCities::model()->getCommandBuilder()->createFindCommand(FycCities::model()->tableSchema, $ccri)->queryAll();

        if (!empty($countrymodel))
        {

            $this->_sendResponse(200, $countrymodel);

        }
        else
        {

            $this->_sendResponse(507, array());

        }
     }

    //// Get subcategoryname
    

    public function actionsubcategoryname()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycSubcategory::model();

        $cri = new CDbCriteria();

        $cri->select = "subcategory";

        $cri->condition = "subcategoryid =$id";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    //// Get search
    

    //// Get search
    

    public function actionsearchfilter()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }

        $key = $_POST['key'];
        $city = $_POST['city'];

        if ($city == '')
        {
            $city = 'villupuram';
        }

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`,
         dir.`directorycontactnumber`, dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`,
          dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`, img.imagename";
        $cri->join = "left outer join fyc_imagegallery as img on img.imagegalleryid = dir.directoryid";
        $cri->condition = "(dir.directorytitle like '%" . $key . "%' and dir.directoryaddress like '%" . $city . "%') and dir.directorystatus = 1 ORDER BY directoryid ASC";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);

        }
        else
        {
            $this->_sendResponse(507, array());

        }

    }

    public function actionsearch()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $categoryid = $_POST['categoryid'];
        $subcategoryid = $_POST['subcategoryid'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $title = isset($_POST['title']) ? $_POST['title'] : "";
        $radius = isset($_POST['radius']) ? $_POST['radius'] : 0;

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "d";

        if ($categoryid > 0) $cri->addCondition("d.categoryid =" . $categoryid);

        if ($subcategoryid > 0) $cri->addCondition("d.subcategoryid =" . $subcategoryid);

        if (trim($title) != "")
        {
            $cri->addCondition("d.title like '%" . $title . "%'");
        }

        if ($latitude > 0 && $longitude > 0)
        {
            if ($radius > 0)
            {
                $cri->select = "*, 
                        (
                            (
                                (
                                    acos(
                                        sin(( $latitude * pi() / 180))
                                        *
                                        sin(( `latitude` * pi() / 180)) + cos(( $latitude * pi() /180 ))
                                        *
                                        cos(( `latitude` * pi() / 180)) * cos((( $longitude - `longitude`) * pi()/180)))
                                ) * 180/pi()
                            ) * 60 * 1.1515 * 1.609344
                        )
                    as distance";
                $cri->having = "distance < " . $radius;
            }
            else
            {
                $cri->select = "*, 
                        (
                            (
                                (
                                    acos(
                                        sin(( $latitude * pi() / 180))
                                        *
                                        sin(( `latitude` * pi() / 180)) + cos(( $latitude * pi() /180 ))
                                        *
                                        cos(( `latitude` * pi() / 180)) * cos((( $longitude - `longitude`) * pi()/180)))
                                ) * 180/pi()
                            ) * 60 * 1.1515 * 1.609344
                        )
                    as distance";
                $cri->having = "distance < " . 25;
            }

        }
        
        

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();
        
        $totarr =array();
        
        foreach($users as $usr){
            
            $cri = new CDbCriteria();
            $cri->condition = "id =".$usr['id'];
            $directory = FycDirectory::model()->getCommandBuilder()->createFindCommand(FycDirectory::model()->tableSchema, $cri)->queryAll();
             
            $dirarr = $directory[0];
             
            $cri = new CDbCriteria();
            $cri->alias = 'r';
            $cri->select = "r.id, r.directoryid, r.memberid, u.firstname, u.lastname, r.rating, r.title, r.comments, r.created, r.modified";
            $cri->condition = "r.directoryid =" . $usr['id'] . "";
            $cri->join = "left outer join fyc_member u on u.memberid = r.memberid";

            $itemRating = FycReview::model()->findAll($cri);

            $ratingNumber = 0;
            $count = 0;
            foreach ($itemRating as $rate)
            {
                $ratingNumber += $rate['rating'];
                $count += 1;

            }
            $average = 0;
            if ($ratingNumber && $count)
            {
                $average = $ratingNumber / $count;
            }
            
            $totarr[] = array_merge($dirarr,array("averageRating"=>round($average, 0)));
             
        }

        if (!empty($totarr))
        {
            $this->_sendResponse(200, $totarr);

        }
        else
        {
            $this->_sendResponse(507, array());

        }

    }

    // public function actionsearch()
    // {

    //     $this->_authenticate();

    //     if (json_decode(file_get_contents("php://input") , true))
    //     {
    //         $_POST = json_decode(file_get_contents("php://input") , true);
    //     }

    //     $categoryid = $_POST['categoryid'];
    //     $subcategoryid = $_POST['subcategoryid'];
    //     $latitude = $_POST['latitude'];
    //     $longitude = $_POST['longitude'];
    //     $title = isset($_POST['title']) ? $_POST['title'] : "";
    //     $radius = isset($_POST['radius']) ? $_POST['radius'] : 0;

    //     $model = FycDirectory::model();

    //     $cri = new CDbCriteria();

    //     $cri->alias = "d";

    //     if ($categoryid > 0) $cri->addCondition("d.categoryid =" . $categoryid);

    //     if ($subcategoryid > 0) $cri->addCondition("d.subcategoryid =" . $subcategoryid);

    //     if (trim($title) != "")
    //     {
    //         $cri->addCondition("d.title like '%" . $title . "%'");
    //     }

    //     if ($latitude > 0 && $longitude > 0)
    //     {
    //         if ($radius > 0)
    //         {
    //             $cri->select = "*, 
    //                     (
    //                         (
    //                             (
    //                                 acos(
    //                                     sin(( $latitude * pi() / 180))
    //                                     *
    //                                     sin(( `latitude` * pi() / 180)) + cos(( $latitude * pi() /180 ))
    //                                     *
    //                                     cos(( `latitude` * pi() / 180)) * cos((( $longitude - `longitude`) * pi()/180)))
    //                             ) * 180/pi()
    //                         ) * 60 * 1.1515 * 1.609344
    //                     )
    //                 as distance";
    //             $cri->having = "distance < " . $radius;
    //         }
    //         else
    //         {
    //             $cri->select = "*, 
    //                     (
    //                         (
    //                             (
    //                                 acos(
    //                                     sin(( $latitude * pi() / 180))
    //                                     *
    //                                     sin(( `latitude` * pi() / 180)) + cos(( $latitude * pi() /180 ))
    //                                     *
    //                                     cos(( `latitude` * pi() / 180)) * cos((( $longitude - `longitude`) * pi()/180)))
    //                             ) * 180/pi()
    //                         ) * 60 * 1.1515 * 1.609344
    //                     )
    //                 as distance";
    //             $cri->having = "distance < " . 25;
    //         }

    //     }
        
        

    //     $users = $model->getCommandBuilder()
    //         ->createFindCommand($model->tableSchema, $cri)->queryAll();
        
    //     $totarr =array();
        
    //     foreach($users as $usr){
            
    //         $cri = new CDbCriteria();
    //         $cri->condition = "status = '1' and id =".$usr['id'];
    //         $directory = FycDirectory::model()->getCommandBuilder()->createFindCommand(FycDirectory::model()->tableSchema, $cri)->queryAll();
             
    //         $dirarr = $directory;
             
    //         $cri = new CDbCriteria();
    //         $cri->alias = 'r';
    //         $cri->select = "r.id, r.id, r.memberid, u.firstname, u.lastname, r.rating, r.title, r.comments, r.created, r.modified";
    //         $cri->condition = "r.id =" . $usr['id'] . " and r.status = '1' ";
    //         $cri->join = "left outer join fyc_member u on u.memberid = r.memberid";

    //         $itemRating = FycReview::model()->findAll($cri);

    //         $ratingNumber = 0;
    //         $count = 0;
    //         foreach ($itemRating as $rate)
    //         {
    //             $ratingNumber += $rate['rating'];
    //             $count += 1;

    //         }
    //         $average = 0;
    //         if ($ratingNumber && $count)
    //         {
    //             $average = $ratingNumber / $count;
    //         }
            
    //         $totarr[] = array_merge($dirarr,array("averageRating"=>round($average, 0)));
             
    //     }

    //     if (!empty($totarr))
    //     {
    //         $this->_sendResponse(200, $totarr);

    //     }
    //     else
    //     {
    //         $this->_sendResponse(507, array());

    //     }

    // }

    public function actiongetallbusinesscards()
    {
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycBusinesscards::model();

        $cri = new CDbCriteria();
        $cri->condition = "directory_id=$id";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actionprofilesearchfilter()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $sid = $_POST['subcategoryid'];
        $city = $_POST['city'];

        $model = FycDirectory::model();
        $cri = new CDbCriteria();
        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`,
         dir.`directorycontactnumber`, dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`verify_label`,
         dir.`premium_label`,dir.`blogger_link`,dir.`single_page_link`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`, img.imagename";
        $cri->join = "left outer join fyc_imagegallery as img on img.directoryid = dir.directoryid";
        if ($city == '0')
        {
            $cri->condition = "(dir.subcategoryid = $sid) and dir.directorystatus = 1 or img.banner_image = 1 group by dir.directoryid";
        }
        else
        {
            $cri->condition = "(dir.cityid =$city and dir.subcategoryid = $sid) and dir.directorystatus = 1 or img.banner_image = 1 group by dir.directoryid";
        }

        $cri->order = "dir.premium_label DESC";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);

        }
        else
        {
            $this->_sendResponse(507, array());

        }

    }

    public function actionupdatecard()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //
        $model = FycBusinesscards::model()->findByPk($id);
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $imgmodel = FycBusinesscards::model()->findByPk($id);

        $logopath = $_POST['logoPath'];
        $oldpath = $_POST['oldpath'];

        if ($oldpath == $imgmodel->logoPath)
        {
            $model->logoPath = $imgmodel->logoPath;
        }
        else
        {

            list($type, $logopath) = explode(';', $logopath);
            $path = "images/card/";
            if (!is_dir($path))
            {
                mkdir($path, 0777, true);
            }
            list(, $extension) = explode('/', $type);
            list(, $logopath) = explode(',', $logopath);
            $fileName = $path . uniqid() . '.' . $extension;
            $imageData = base64_decode($logopath);
            file_put_contents($fileName, $imageData);

            $model->logoPath = $fileName;
        }

        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'id,name';
            $cri->condition = "id=" . $model->id;
            $users = FycBusinesscards::model()->getCommandBuilder()
                ->createFindCommand(FycBusinesscards::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
    }

    public function actioncreatecard()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $directoryid = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //
        $model = new FycBusinesscards();
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $logopath = $_POST['logoPath'];

        list($type, $logopath) = explode(';', $logopath);
        $path = "images/card/";
        if (!is_dir($path))
        {
            mkdir($path, 0777, true);
        }
        list(, $extension) = explode('/', $type);
        list(, $logopath) = explode(',', $logopath);
        $fileName = $path . uniqid() . '.' . $extension;
        $imageData = base64_decode($logopath);
        file_put_contents($fileName, $imageData);

        $model->logoPath = $fileName;
        $model->directory_id = $directoryid;

        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'id,name';
            $cri->condition = "id=" . $model->id;
            $users = FycBusinesscards::model()->getCommandBuilder()
                ->createFindCommand(FycBusinesscards::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
    }
    
    public function actionsavecardcategory()
    {
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        
        $card_id = $_POST['card_id'];
        $memberid = $_POST['memberid'];
        $cardcategory_id = $_POST['cardcategory_id'];
        
        $model = FycSavecardCategories::model()->findByAttributes(array("memberid"=>$memberid, "card_id" => $card_id));
        
        if($model == null)
        {
            $model = new FycSavecardCategories();
            
            $model->card_id = $card_id;
            $model->memberid = $memberid;
            $model->cardcategory_id = $cardcategory_id;
            
            $model->save();
        }else{
            FycSavecardCategories::model()->deleteAllByAttributes(array("memberid"=>$memberid, "card_id" => $card_id));
            
            $model = new FycSavecardCategories();
            
            $model->card_id = $card_id;
            $model->memberid = $memberid;
            $model->cardcategory_id = $cardcategory_id;
            
            $model->save();
        }
        
         $this->_sendResponse(200, array());
    }
    
    
    public function actioncreatecardcategory()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $memberid = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //
        $model = new FycCardCategories();
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $model->memberid = $memberid;

        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'id,name,memberid, created';
            $cri->condition = "memberid=" . $model->memberid;
            $users = FycCardCategories::model()->getCommandBuilder()
                ->createFindCommand(FycCardCategories::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
    }

    //// Get profilelist
    

    public function actionprofilelist()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`, dir.`directorycontactnumber`,dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`, img.imagename";
        $cri->join = "left outer join fyc_imagegallery as img on img.directoryid = dir.directoryid";
        $cri->condition = "dir.subcategoryid = '$id' and dir.directorystatus = 1";
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actionprofilelistold()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`, dir.`directorycontactnumber`,dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`, img.imagename as image";
        $cri->join = "left outer join fyc_imagegallery as img on img.directoryid = dir.directoryid";
        $cri->condition = "dir.subcategoryid = '$id' and dir.directorystatus = 1";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if ($users[0]['image'] == NULL)
        {

            $users[0]['image'] = "../02.jpg";
        }

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actiongetdirectory()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $userid = $_GET['userid'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();
        $cri->alias = "dir";
        $cri->select = "dir.*, usr.firstname, usr.lastname, Count(f.id) as favCount";
        $cri->join = "left outer join fyc_member as usr on usr.memberid = dir.memberid
                        left outer join fyc_favourite f on f.directory_id = dir.id and f.memberid = '$userid'";
        $cri->condition = "dir.id = '$id' and dir.status = 1";
        $cri->group = "dir.id";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();
        
        $dirmodel = FycDirectory::model()->findByPk($id);
        
        $countrymodel = FycCountries::model()->findByAttributes(array('name'=> $dirmodel->country));
        $statemodel = FycStates::model()->findByAttributes(array('name'=> $dirmodel->state));
        $citymodel = FycCities::model()->findByAttributes(array('name'=> $dirmodel->town));
        
        $totalarray = array();
        
        $totalarray = array_merge($users[0], array("countryid"=>($countrymodel == null ? 101 : $countrymodel->id), "stateid"=>($statemodel == null ? 35 : $statemodel->id), "cityid"=>($citymodel == null ? 0 : $citymodel->id)));

        if (!empty($totalarray))
        {
            $this->_sendResponse(200, $totalarray);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    public function actiongetcard()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycBusinesscards::model();

        $cri = new CDbCriteria();
        $cri->condition = "id = '$id'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    //// Get profiledesc
    

    public function actionprofiledesc()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`, dir.`directorycontactnumber`,dir.`cityid`, dir.`verify_label`,dir.`premium_label`,dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, 
            dir.`meta_title`,dir.`meta_description`,dir.`keywords`,dir.`blogger_link`,dir.`single_page_link`,dir.`memberid`, img.imagename, img.banner_image as image";
        $cri->join = "left outer join fyc_imagegallery as img on img.directoryid = dir.directoryid";
        $cri->condition = "dir.directoryid = '$id' and dir.directorystatus = 1";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    public function actionsubmitreview()
    {
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $userid = $_GET['id'];

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $model = new FycReview();

        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $model->created = $now->format('Y-m-d H:i:s');
        $model->memberid = $userid;
        $model->status = '1';
        if ($model->save())
        {
            $this->_sendResponse(200, array());
        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    public function actionaddfavourite()
    {
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $userid = $_POST['memberid'];
        $profileid = $_POST['directory_id'];

        $model = FycFavourite::model()->findByAttributes(array(
            "memberid" => $userid,
            "directory_id" => $profileid
        ));

        $now = new DateTime();

        if ($model == null)
        {
            $model = new FycFavourite();

            $model->memberid = $userid;
            $model->directory_id = $profileid;
            $model->created = $now->format('Y-m-d H:i:s');

            if ($model->save())
            {
                $this->_sendResponse(200, array(
                    "message" => "added"
                ));
            }
            else
            {
                $this->_sendResponse(507, array(
                    "message" => $model->errors
                ));
            }
        }
        else
        {
            if ($model->delete())
            {
                $this->_sendResponse(200, array(
                    "message" => "deleted"
                ));
            }
            else
            {
                $this->_sendResponse(507, array(
                    "message" => $model->errors
                ));
            }
        }

    }

    ////  review
    public function actionreview()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $id = $_POST['directoryid'];
        $model = new FycReview();

        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                $this->_sendresponse(400, array());
            }
        }

        $rating_plus = 0;
        $i = 0;
        $model->date = $now->format('Y-m-d H:i:s');
        if ($model->save())
        {
            $data = FycReview::model()->findByAttributes(array(
                "directoryid" => $id
            ));
            foreach ($data as $key => $value)
            {
                if ($key == 'rating')
                {
                    $rating_plus += intva($value);
                    $i++;
                }
            }

            $overall_rate = $rating_plus / $i;
            $dirmodel = FycDirectory::model()->findByPk($id);
            $dirmodel->rating = $overall_rate;

            if ($dirmodel->save())
            {
                $this->_sendResponse(200, array());
            }
            else
            {
                $this->_sendResponse(507, array());
            }
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    // Get images
    public function actiongetimages()
    {
        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $model = new FycImagegallery();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "directoryid = '$id'";
        $cri->limit = "3";
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    //// Get review
    

    public function actionviewreview()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycReview::model();

        $cri = new CDbCriteria();

        $cri->alias = "rev";

        $cri->select = "*";
        $cri->condition = "directoryid = '$id'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    //// Get review by user id
    

    public function actionreviewlist()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycReview::model();

        $cri = new CDbCriteria();

        $cri->alias = "r";

        $cri->select = "r.id, r.memberid, r.directoryid, r.title, r.comments, r.status, r.rating, r.created, d.title";
        $cri->join = " left outer join fyc_directory d ON r.directoryid = d.id";
        $cri->condition = "r.memberid = '$id'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }
    
    public function actiongetsavedcardlist()
    {

//        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $catid = $_GET['catid'];
        
        $cri = new CDbCriteria();
        $cri->alias = 'cs';
        $cri->select = "cs.id, c.name, u.lastname, cs.card_id, cs.created";
        $cri->condition = "cs.memberid =" . $id . " and cs.cardcategory_id=$catid";
        $cri->join = "left outer join fyc_member u on u.memberid = cs.memberid
                      left outer join fyc_businesscards c on c.id = cs.card_id";

        $users = FycSavecardCategories::model()->getCommandBuilder()
            ->createFindCommand(FycSavecardCategories::model()->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

      
    }
    
     //// Get review by user id
    

    public function actionfavouritelist()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycFavourite::model();

        $cri = new CDbCriteria();

        $cri->alias = "f";

        $cri->select = "f.*, d.title";
        $cri->join = " left outer join fyc_directory d ON f.directory_id = d.id";
        $cri->condition = "f.memberid = '$id'";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actionnewreview()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $userid = $_POST['userid'];

        $member = FycMember::model()->findByPk($userid);

        // $email = $_POST['email'];
        $model = FycReview::model()->findByAttributes(array(
            'memberid' => $member->memberid
        ));
        if (empty($model))
        {
            $model = new FycReview();

        }

        $now = new DateTime();
        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $rating_plus = 0;

        $model->memberid = $userid;
        $model->email = $member->email;
        $model->name = $member->firstname;
        $model->phone = $member->telephone;
        $model->date = $now->format('Y-m-d H:i:s');
        if ($model->save())
        {

            $cri = new CDbCriteria();
            $review = new FycReview();

            $cri->alias = "rev";
            $cri->select = "rev.rating";
            $cri->condition = "rev.directoryid = '" . $model->directoryid . "'";
            $users = $review->getCommandBuilder()
                ->createFindCommand($review->tableSchema, $cri)->queryAll();

            for ($i = 0;$i <= count($users) - 1;$i++)
            {
                $rating_plus += intval($users[$i]['rating']);
            }

            $overall_rate = $rating_plus / $i;

            $dirmodel = FycDirectory::model()->findByPk($model->directoryid);
            $dirmodel->rating = $overall_rate;

            if ($dirmodel->save())
            {
                $this->_sendResponse(200, array());
            }
            else
            {
                // echo var_dump($dirmodel->getErrors());
                $this->_sendResponse(500, array());
            }
        }
        else
        {
            $this->_sendResponse(507, array());

        }

    }

    public function actionpercentreview()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = new FycReview();

        $cri1 = new CDbCriteria();
        $cri1->select = "count(rating) as rate1";
        $cri1->condition = "rating = '1.0' and directoryid = '" . $id . "'";
        $models1 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri1)->queryAll();

        $cri2 = new CDbCriteria();
        $cri2->select = "count(rating) as rate2";
        $cri2->condition = "rating = '2.0' and directoryid = '" . $id . "'";
        $models2 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri2)->queryAll();

        $cri3 = new CDbCriteria();
        $cri3->select = "count(rating) as rate3";
        $cri3->condition = "rating = '3.0' and directoryid = '" . $id . "'";
        $models3 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri3)->queryAll();

        $cri4 = new CDbCriteria();
        $cri4->select = "count(rating) as rate4";
        $cri4->condition = "rating = '4.0' and directoryid = '" . $id . "'";
        $models4 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri4)->queryAll();

        $cri5 = new CDbCriteria();
        $cri5->select = "count(rating) as rate5";
        $cri5->condition = "rating = '5.0' and directoryid = '" . $id . "'";
        $models5 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri5)->queryAll();

        $cri6 = new CDbCriteria();
        $cri6->select = "count(rating) as allrate";
        $cri6->condition = "directoryid = '" . $id . "'";
        $models6 = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri6)->queryAll();

        $data['rate1'] = $models1[0]['rate1'];
        $data['rate2'] = $models2[0]['rate2'];
        $data['rate3'] = $models3[0]['rate3'];
        $data['rate4'] = $models4[0]['rate4'];
        $data['rate5'] = $models5[0]['rate5'];
        $data['allrate'] = $models6[0]['allrate'];

        if (!empty($data))
        {
            $this->_sendResponse(200, $data);

        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    /// delete review
    public function actiondeletereview()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        FycReview::model()->deleteAll($cri);
        $this->_sendResponse(200, "Delete Successfull");
    }
    
    /// delete review
    public function actiondeletefavourite()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        FycFavourite::model()->deleteAll($cri);
        $this->_sendResponse(200, "Delete Successfull");
    }

    ///     public function actiondeletedirectory()
    public function actiondeletedirectory()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $model = FycDirectory::model()->findByPk($id);

        $model->status = '2';
        $model->save();

        $this->_sendResponse(200, "Delete Successfull");

        //        $id = $_GET['id'];
        //        $cri = new CDbCriteria();
        //        $cri->condition = "id = ".$_GET['id'];
        //        FycDirectory::model()->deleteAll($cri);
        //        $this->_sendResponse(200, "Delete Successfull");
        
    }

    //// Get viewprofile
    

    public function actionviewprofile()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`, dir.`directorycontactnumber`,dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`, img.imagename ,sub.subcategory, cat.category";
        $cri->join = "left outer join fyc_imagegallery as img on img.imagegalleryid = dir.directoryid
                        left outer join fyc_subcategory as sub on sub.subcategoryid = dir.subcategoryid
                        left outer join fyc_category as cat on cat.categoryid = sub.categoryid";

        $cri->condition = "dir.memberid =$id and dir.directorystatus = 1";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    //// Get viewprofile
    

    public function actionfetchprofile()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = "dir";

        $cri->select = "dir.`directoryid`, dir.`subcategoryid`, dir.`directorytitle`, dir.`directorystatus`, dir.`directorydescription`, dir.`directoryaddress`, dir.`directorylandmark`, dir.`directorycontactname`, dir.`directorycontactnumber`,dir.`cityid`, dir.`directoryzipcode`, dir.`directoryurl`, dir.`directoryemail`, dir.`directoryaudio`, dir.`directoryvideo`, dir.`directoryimage`, dir.`videocount`, dir.`imagecount`, dir.`createdby`, dir.`createddatetime`, dir.`updatedby`, dir.`updateddatetime`, dir.`cost`, dir.`rating`, dir.`isadmin`, dir.`approve`, dir.`sourcecountryid`, dir.`countryid`, dir.`memberid`,cat.categoryid, dir.imagename";
        $cri->join = "left outer join fyc_imagegallery as img on img.imagegalleryid = dir.directoryid
                        left outer join fyc_subcategory as sub on sub.subcategoryid = dir.subcategoryid
                        left outer join fyc_category as cat on cat.categoryid = sub.categoryid";

        $cri->condition = "dir.directoryid =$id and dir.directorystatus = 1";
        // echo var_dump($cri);
        // die();
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {

            $this->_sendResponse(200, $users);

        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    public function actioncategoryid()
    {

        $this->_authenticate();

        $id = $_GET['id'];
        $model = FycDirectory::model();
        $cri = new CDbCriteria();
        $cri->select = "subcategoryid";
        $cri->condition = "directoryid = '" . $id . "'";
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actioncreatedirectory()
    {
        //        echo "succesffffs";
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //          echo var_dump($_POST);
        //        die();
        //
        $model = new FycDirectory();
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $logopath = $_POST['logo'];

        list($type, $logopath) = explode(';', $logopath);
        $path = "images/logo/";
        if (!is_dir($path))
        {
            mkdir($path, 0777, true);
        }
        list(, $extension) = explode('/', $type);
        list(, $logopath) = explode(',', $logopath);
        $fileName = $path . uniqid() . '.' . $extension;
        $imageData = base64_decode($logopath);
        file_put_contents($fileName, $imageData);

        $model->logo = $fileName;

        $coverpath = $_POST['cover'];

        list($type, $coverpath) = explode(';', $coverpath);
        $path = "images/cover/";
        if (!is_dir($path))
        {
            mkdir($path, 0777, true);
        }
        // chmod($path, 0755);
        list(, $extension) = explode('/', $type);
        list(, $coverpath) = explode(',', $coverpath);
        $fileName = $path . uniqid() . '.' . $extension;
        $imageData = base64_decode($coverpath);
        file_put_contents($fileName, $imageData);

        $model->country = FycCountries::model()->findByPk($_POST['country'])->name;
        $model->state = FycStates::model()->findByPk($_POST['state'])->name;
        $model->town = FycCities::model()->findByPk($_POST['town'])->name;
        $model->status = '0';
        $model->isadmin = '0';
        $model->approve = '0';
        $model->cover = $fileName;
        $model->createddatetime = $now->format('Y-m-d H:i:s');
        if ($model->save())
        {

           $cardmodel = new FycBusinesscards();
           
           $cardmodel->name = $model->title;
           $cardmodel->memberid = $model->memberid;
           $cardmodel->directory_id = $model->id;
           $cardmodel->email = $model->email;
           $cardmodel->phone = $model->phone;
           $cardmodel->address = $model->address;
           $cardmodel->save();
           
           
            $cri = new CDbCriteria();

            $cri->select = 'max(id) as id,title';
            $cri->condition = "id =" . $model->id;
            $users = FycDirectory::model()->getCommandBuilder()
                ->createFindCommand(FycDirectory::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
        //
        
    }

    public function actionsendcard()
    {
        //
        //             echo "succesffffs";
        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //          echo var_dump($_POST);
        //        die();
        //
        $model = new FycCardSendItems();
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }
        $model->createdAt = $now->format('Y-m-d H:i:s');
        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'max(id) as id';
            $cri->condition = "id =" . $model->id;
            $users = FycCardSendItems::model()->getCommandBuilder()
                ->createFindCommand(FycCardSendItems::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
        //
        
    }

    public function actionupdateprofile()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        //
        $model = FycMember::model()->findByPk($id);
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $imgmodel = FycMember::model()->findByPk($id);

        $logopath = $_POST['photo'];
        $oldpath = $_POST['oldpath'];

        if ($oldpath == $imgmodel->photo)
        {
            $model->photo = $imgmodel->photo;
        }
        else
        {

            list($type, $logopath) = explode(';', $logopath);
            $path = "images/user/";
            if (!is_dir($path))
            {
                mkdir($path, 0777, true);
            }
            list(, $extension) = explode('/', $type);
            list(, $logopath) = explode(',', $logopath);
            $fileName = $path . uniqid() . '.' . $extension;
            $imageData = base64_decode($logopath);
            file_put_contents($fileName, $imageData);

            $model->photo = $fileName;
        }

        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'memberid,firstname';
            $cri->condition = "memberid=" . $model->memberid;
            $users = FycMember::model()->getCommandBuilder()
                ->createFindCommand(FycMember::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
        //
        
    }

    public function actionupdatedirectory()
    {
        //        echo "succesffffs";
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        //          echo var_dump($_POST);
        //        die();
        //
        $model = FycDirectory::model()->findByPk($id);
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                // $this->_sendresponse(400, array());
                
            }
        }

        $imgmodel = FycDirectory::model()->findByPk($id);

        $logopath = $_POST['logo'];

        if ($this->is_base64($logopath))
        {
            list($type, $logopath) = explode(';', $logopath);
            $path = "images/logo/";
            if (!is_dir($path))
            {
                mkdir($path, 0777, true);
            }
            list(, $extension) = explode('/', $type);
            list(, $logopath) = explode(',', $logopath);
            $fileName = $path . uniqid() . '.' . $extension;
            $imageData = base64_decode($logopath);
            file_put_contents($fileName, $imageData);

            $model->logo = $fileName;
        }
        else
        {
            $model->logo = $imgmodel->logo;
        }

        $coverpath = $_POST['cover'];

        if ($this->is_base64($coverpath))
        {
            list($type, $coverpath) = explode(';', $coverpath);
            $path = "images/cover/";
            if (!is_dir($path))
            {
                mkdir($path, 0777, true);
            }
            // chmod($path, 0755);
            list(, $extension) = explode('/', $type);
            list(, $coverpath) = explode(',', $coverpath);
            $fileName = $path . uniqid() . '.' . $extension;
            $imageData = base64_decode($coverpath);
            file_put_contents($fileName, $imageData);

            $model->cover = $fileName;
        }
        else
        {
            $model->cover = $imgmodel->cover;
        }

        $model->country = FycCountries::model()->findByPk($_POST['country'])->name;
        $model->state = FycStates::model()->findByPk($_POST['state'])->name;
        $model->town = FycCities::model()->findByPk($_POST['town'])->name;
        $model->updateddatetime = $now->format('Y-m-d H:i:s');
        if ($model->save())
        {

            $cri = new CDbCriteria();

            $cri->select = 'max(id) as id,title';
            $cri->condition = "id =" . $model->id;
            $users = FycDirectory::model()->getCommandBuilder()
                ->createFindCommand(FycDirectory::model()->tableSchema, $cri)->queryAll();

            $this->_sendResponse(200, $users);

        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }
        //
        
    }

    public function actiongetitemrating()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $cri = new CDbCriteria();
        $cri->alias = 'r';
        $cri->select = "r.id, r.directoryid, r.memberid, u.firstname, u.lastname, r.rating, r.title, r.comments, r.created, r.modified";
        $cri->condition = "r.directoryid =" . $id . "";
        $cri->join = "left outer join fyc_member u on u.memberid = r.memberid";

        $itemRating = FycReview::model()->findAll($cri);

        $ratingNumber = 0;
        $count = 0;
        $fiveStarRating = 0;
        $fourStarRating = 0;
        $threeStarRating = 0;
        $twoStarRating = 0;
        $oneStarRating = 0;
        foreach ($itemRating as $rate)
        {
            $ratingNumber += $rate['rating'];
            $count += 1;
            if ($rate['rating'] == 5)
            {
                $fiveStarRating += 1;
            }
            else if ($rate['rating'] == 4)
            {
                $fourStarRating += 1;
            }
            else if ($rate['rating'] == 3)
            {
                $threeStarRating += 1;
            }
            else if ($rate['rating'] == 2)
            {
                $twoStarRating += 1;
            }
            else if ($rate['rating'] == 1)
            {
                $oneStarRating += 1;
            }
        }
        $average = 0;
        if ($ratingNumber && $count)
        {
            $average = $ratingNumber / $count;
        }

        $fiveStarRatingPercent = round(($fiveStarRating / 5) * 100);
        $fiveStarRatingPercent = !empty($fiveStarRatingPercent) ? $fiveStarRatingPercent . '%' : '0%';

        $fourStarRatingPercent = round(($fourStarRating / 5) * 100);
        $fourStarRatingPercent = !empty($fourStarRatingPercent) ? $fourStarRatingPercent . '%' : '0%';

        $threeStarRatingPercent = round(($threeStarRating / 5) * 100);
        $threeStarRatingPercent = !empty($threeStarRatingPercent) ? $threeStarRatingPercent . '%' : '0%';

        $twoStarRatingPercent = round(($twoStarRating / 5) * 100);
        $twoStarRatingPercent = !empty($twoStarRatingPercent) ? $twoStarRatingPercent . '%' : '0%';

        $oneStarRatingPercent = round(($oneStarRating / 5) * 100);
        $oneStarRatingPercent = !empty($oneStarRatingPercent) ? $oneStarRatingPercent . '%' : '0%';

        $data = array();

        $data['count'] = $count;
        $data['fiveStarRating'] = $fiveStarRating;
        $data['fourStarRating'] = $fourStarRating;
        $data['threeStarRating'] = $threeStarRating;
        $data['twoStarRating'] = $twoStarRating;
        $data['oneStarRating'] = $oneStarRating;
        $data['fiveStarRatingPercent'] = $fiveStarRatingPercent;
        $data['fourStarRatingPercent'] = $fourStarRatingPercent;
        $data['threeStarRatingPercent'] = $threeStarRatingPercent;
        $data['twoStarRatingPercent'] = $twoStarRatingPercent;
        $data['oneStarRatingPercent'] = $oneStarRatingPercent;
        $data['average'] = number_format($average, 1);
        $data['averageRating'] = round($average, 0);

        $this->_sendResponse(200, $data);

    }

    public function actiongetallreviewrating()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $cri = new CDbCriteria();
        $cri->alias = 'r';
        $cri->select = "r.id, r.directoryid, r.memberid, u.firstname, u.lastname, r.rating, r.title, r.comments, r.created, r.modified";
        $cri->condition = "r.directoryid =" . $id . "";
        $cri->join = "left outer join fyc_member u on u.memberid = r.memberid";
        $cri->order = "r.id DESC ";

        $users = FycReview::model()->getCommandBuilder()
            ->createFindCommand(FycReview::model()->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

        //        $itemRating = FycReview::model()->findAll($cri);
        

        
    }

    public function actiongetcardlist()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $cri = new CDbCriteria();
        $cri->alias = 'cs';
        $cri->select = "cs.id, u.firstname as name, u.lastname, cs.card_id, cs.createdAt, cs.subject";
        $cri->condition = "cs.touser_id =" . $id . "";
        $cri->join = "left outer join fyc_member u on u.memberid = cs.fromuser_id
                      left outer join fyc_businesscards c on c.id = cs.card_id";

        $users = FycCardSendItems::model()->getCommandBuilder()
            ->createFindCommand(FycCardSendItems::model()->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }

        //        $itemRating = FycReview::model()->findAll($cri);
        

        
    }

    public function is_base64($s)
    {
        // Check if there are valid base64 characters
        if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s)) return false;

        // Decode the string in strict mode and check the results
        $decoded = base64_decode($s, true);
        if (false === $decoded) return false;

        // Encode the string again
        if (base64_encode($decoded) != $s) return false;

        return true;
    }

    ////  createprofile
    

    public function actioncreateprofile()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0)

        $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }
        $id = $_GET['id'];

        $model = new FycDirectory();
        // $model1 = FycDirectory::model()->findByAttributes(array('directoryid' => $id));
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {
            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                $this->_sendresponse(400, array());
            }
        }
        $gpath = $_POST['imagename'];
        $path = "images/profiles/";
        $image_parts = explode(";base64,", $gpath);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $path . uniqid() . '.jpeg';
        file_put_contents($file, $image_base64);
        $model->imagename = $file;

        $model->createddatetime = $now->format('Y-m-d H:i:s');
        $model->memberid = $id;
        if ($model->save())
        {
            $cri = new CDbCriteria();

            $cri->select = 'max(directoryid) as directoryid,directorytitle,imagename';
            $cri->condition = "memberid =" . $id;
            $cri->group = "memberid";
            $cri->order = "directoryid DESC ";

            $users = $model->getCommandBuilder()
                ->createFindCommand($model->tableSchema, $cri)->queryAll();

            $model1 = FycDirectory::model()->findByAttributes(array(
                'directoryid' => $users[0]['directoryid']
            ));
            $model2 = new FycImagegallery();
            //    $model1 = FycDirectory::model()->findByAttributes(array('directoryid' => $users[0]['directoryid']));
            //    $model2 = FycImagegallery::model()->findByAttributes(array('directoryid' => $users[0]['directoryid']));
            $model2->imagename = $model1->imagename;
            $model2->directoryid = $model1->directoryid;
            $model2->banner_image = '1';
            $model2->save();

            // echo var_dump($users);
            // die();
            $this->_sendResponse(200, $users);
        }
        else
        {
            echo var_dump($model->errors);
            $this->_sendResponse(507, array());

        }

    }

    public function actionimageupload()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $id = $_GET['id'];
        $gpath = $_POST['imagename'];

        $countmodel = FycImagegallery::model()->countByAttributes(array(
            'directoryid' => $_GET['id']
        ));
        if ($countmodel <= 3)
        {

            $model = new FycImagegallery();

            $path = "images/profiles/";

            $image_parts = explode(";base64,", $gpath);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $path . uniqid() . '.jpeg';

            file_put_contents($file, $image_base64);

            foreach ($_POST as $var => $value)
            {
                if ($model->hasAttribute($var))
                {
                    $model->$var = $value;
                }
                else
                {
                    $this->_sendresponse(400, array());
                }
            }

            // $model->banner_image = 0;
            $model->imagename = $file;
            $model->directoryid = $_GET['id'];
            if ($model->save())
            {
                $this->_sendresponse(200, sprintf("image uploded successfully"));
            }
            else
            {
                $this->_sendresponse(500, sprintf("image uploded successfully"));
            }
        }
        else
        {
            $this->_sendresponse(500, sprintf("more than image"));
        }

    }

    ////  updateprofile
    /*update banner*/
    public function actionupdatebanner()
    {

        $this->_authenticate();

        if (!isset($_GET['imgid']) || $_GET['imgid'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['imgid'];
        $did = $_GET['dirid'];
        $model = FycImagegallery::model()->findByPk($id);
        $model1 = FycImagegallery::model()->findByAttributes(array(
            'directoryid' => $did
        ));
        $model1->banner_image = 0;
        if ($model1->save())
        {
            $model->banner_image = 1;
            if ($model->save())
            {
            }
            else
            {
                $this->_sendresponse(500, sprintf("Internal server error1"));
            }
        }
        else
        {
            $this->_sendresponse(500, sprintf("Internal server error"));
        }
    }
    /*end update banner*/

    /*delete photo*/
    public function actiondeletephoto()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycImagegallery::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "imagegalleryid=" . $id;
        $modell = FycImagegallery::model()->deleteAll($cri);
        if ($model->save())
        {
            $this->_sendresponse(200, sprintf("image deleted successfully"));
        }
        else
        {
            $this->_sendresponse(500, sprintf("failed"));
        }

    }
    /*delete photo*/

    //
    //      public function actionupdateprofile() {
    //
    //        if (!isset($_GET['id']) || $_GET['id'] == 0)
    //
    //       $this->_sendResponse(404, array());
    //       if (json_decode(file_get_contents("php://input"), true)) {
    //
    //        $put_vars = json_decode(file_get_contents("php://input"), true);
    //        }else{
    //
    //            $put_vars = $_POST;
    //        }
    //
    //        $id = $_GET['id'];
    //
    //        $model = FycDirectory::model()->findByPk($_GET['id']);
    //        // $model1 = FycDirectory::model()->findByAttributes(array('directoryid' => $id));
    //
    //        $now = new DateTime();
    //
    //
    //        if($model==null){
    //            $model = new FycDirectory();
    //            $model->userid = $_GET['id'];
    //           }
    //
    //        foreach ($put_vars as $var => $value) {
    //
    //            if ($model->hasAttribute($var)) {
    //
    //                $model->$var = $value;
    //                if($var == 'imagename') {
    //                    if(strlen($value) > 100) {
    //
    //                        $gpath = $_POST['imagename'];
    //                        $path = "images/profiles/";
    //                        $image_parts = explode(";base64,", $gpath);
    //                        $image_type_aux = explode("image/", $image_parts[0]);
    //                        $image_type = $image_type_aux[1];
    //                        $image_base64 = base64_decode($image_parts[1]);
    //                        $file = $path . uniqid() . '.jpeg';
    //                        file_put_contents($file, $image_base64);
    //                        $model->imagename=$file;
    //
    //                    }
    //
    //                }
    //
    //            }
    //            else {
    //                $this->_sendresponse(400, array());
    //            }
    //        }
    //
    //        $model->createddatetime = $now->format('Y-m-d H:i:s');
    //
    //
    //        if ($model->save()){
    //
    //                $model = FycDirectory::model()->findByAttributes(array('directoryid' => $model->directoryid));
    //                $model2 = FycImagegallery::model()->findByAttributes(array('directoryid' => $model->directoryid));
    //
    //                $model2->imagename = $model->imagename;
    //                $model2->directoryid = $model->directoryid;
    //
    //                $model2->banner_image = '1';
    //                $model2->save();
    //
    //                $this->_sendResponse(200, $users);
    //         } else {
    //
    //            $this->_sendResponse(507, array());
    //
    //        }
    //
    //    }
    

    /// delete profile
    public function actiondeleteprofile()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $cri = new CDbCriteria();
        $cri->condition = "directoryid =" . $_GET['id'];
        FycDirectory::model()->deleteAll($cri);
        $this->_sendResponse(200, "Delete Successfull");
    }

    ////  enquiry
    

    public function actionenquiry()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }

        $email = $_POST['email'];
        $name = $_POST['name'];
        $message = $_POST['message'];
        $model = new FycEnquiry();
        $now = new DateTime();

        foreach ($_POST as $var => $value)
        {

            if ($model->hasAttribute($var))
            {
                $model->$var = $value;
            }
            else
            {
                $this->_sendresponse(400, array());
            }

        }

        if ($model->save())
        {
            $this->notifyenquiry($email, $name, $message);
            $cri = new CDbCriteria();

            $cri->select = 'id';
            $users = $model->getCommandBuilder()
                ->createFindCommand($model->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        }
        else
        {

            $this->_sendResponse(507, array());

        }

    }

    /// forgotpassword
    public function actionforgotpassword()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }

        $digits_needed = 6;
        $email = $_POST['email'];
        $random_number = '';
        $count = 0;

        while ($count < $digits_needed)
        {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }

        $model = FycMember::model()->findByAttributes(array(
            'email' => $email
        ));
        if (count($model) > 0)
        {
            $this->notifysentotp($email, $random_number);
            $model->otp_code = $random_number;
            $model->save();
            $this->_sendResponsewithMessage(200, sprintf("Mail Sent Successfully!", ""));
        }
        else
        {
            $this->_sendResponse(507, array());
        }

    }

    public function actioncheckotp()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }
        $email = $_POST['email'];
        $code = $_POST['otp_code'];

        $model = FycMember::model()->findByAttributes(array(
            'email' => $email,
            'otp_code' => $code
        ));

        if (count($model) > 0)
        {
           $this->_sendResponse(200, array());
        }
        else
        {
            $this->_sendResponsewithMessage(507, sprintf("OTP Enterd is wrong !", array()));
        }

    }
    
    public function actioncheckemail()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {

            $_POST = json_decode(file_get_contents("php://input") , true);

        }
        $email = $_POST['email'];

        $model = FycMember::model()->findByAttributes(array(
            'email' => $email,
        ));

        if (count($model) == 0)
        {
           $this->_sendResponse(200, array());
        }
        else
        {
            $this->_sendResponsewithMessage(507, sprintf("Email already taken !", array()));
        }

    }

    // reset otp
    public function actionresetpassword()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        
        $model = FycMember::model()->findByAttributes(array(
            'email' => $email
        ));
        if (count($model) > 0)
        {
            $model->password = $password;
            $model->otp_code = '';
            $model->save();
            $this->_sendResponsewithMessage(200, sprintf("Password Changed Successfully!", ""));
        }
        else
        {
            $this->_sendResponsewithMessage(507, sprintf("Password not changed!", ""));
        }
    }

    public function actiongetalldirectories()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->select = "id, CONCAT(UCASE(MID(title,1,1)),LCASE(MID(title,2))) AS name";
        $cri->condition = "status = 1";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetalldirectorylist()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = 'd';
        $cri->select = "d.id as id, CONCAT(UCASE(MID(d.title,1,1)),LCASE(MID(d.title,2))) AS name, d.address, d.country, d.latitude, d.longitude, d.state, Count(r.id) as reviewCount";
        $cri->condition = "d.status = 1 and d.memberid=$id";
        $cri->join = "left outer join fyc_review r on r.directoryid = d.id";
        $cri->group = 'd.id';
// print_r($cri);die;
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetallpendingdirectorylist()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = 'd';
        $cri->select = "d.id as id, CONCAT(UCASE(MID(d.title,1,1)),LCASE(MID(d.title,2))) AS name, d.address, d.country, d.latitude, d.longitude, d.state, Count(r.id) as reviewCount";
        $cri->condition = "d.status = 0 and d.memberid=$id";
        $cri->join = "left outer join fyc_review r on r.directoryid = d.id";
        $cri->group = 'd.id';

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetalldeleteddirectorylist()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycDirectory::model();

        $cri = new CDbCriteria();

        $cri->alias = 'd';
        $cri->select = "d.id as id, CONCAT(UCASE(MID(d.title,1,1)),LCASE(MID(d.title,2))) AS name, d.address, d.country, d.latitude, d.longitude, d.state, Count(r.id) as reviewCount";
        $cri->condition = "d.status = 2 and d.memberid=$id";
        $cri->join = "left outer join fyc_review r on r.directoryid = d.id";
        $cri->group = 'd.id';

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetallusers()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $model = FycMember::model();

        $cri = new CDbCriteria();

        $cri->select = "memberid as id, CONCAT(UCASE(MID(firstname,1,1)),LCASE(MID(firstname,2))) AS name";
        $cri->condition = "memberstatus = 2 and memberid <> $id";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetallcategories()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $model = FycCategory::model();

        $cri = new CDbCriteria();

        $cri->select = "categoryid as id, CONCAT(UCASE(MID(category,1,1)),LCASE(MID(category,2))) AS name, icon";
        $cri->condition = "categorystatus = 1";

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetallcategorieshome()
    {

        $this->_authenticate();

        if (json_decode(file_get_contents("php://input") , true))
        {
            $_POST = json_decode(file_get_contents("php://input") , true);
        }

        $model = FycSubcategory::model();

        $cri = new CDbCriteria();

        $cri->alias = 's';
        $cri->select = "s.subcategoryid,s.categoryid, CONCAT(UCASE(MID(s.subcategory,1,1)),LCASE(MID(s.subcategory,2))) AS name";
        /*, Count(d.id) as dirCount */
        $cri->condition = "s.subcategorystatus = 1 and s.categoryid=61";
        $cri->join = "left outer join fyc_directory d on d.subcategoryid = s.subcategoryid";
        $cri->group = 's.subcategoryid';
        // $cri->order = 'dirCount DESC';
        $cri->limit = 12;
        $cri->offset = 0;

        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetallcounting()
    {

        $this->_authenticate();

        $dirmodel = FycDirectory::model()->findAllByAttributes(array(
            "status" => "1"
        ));

        $usrmodel = FycMember::model()->findAllByAttributes(array(
            "memberstatus" => "2"
        ));

        $catmodel = FycSubcategory::model()->findAllByAttributes(array(
            "subcategorystatus" => "1"
        ));

        $body = array();

        $body['dir_count'] = count($dirmodel);
        $body['usr_count'] = count($usrmodel);
        $body['cat_count'] = count($catmodel);

        if (!empty($body))
        {
            $this->_sendResponse(200, $body);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetalldashboardcounting()
    {
        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $publmodel = FycDirectory::model()->findAllByAttributes(array(
            "status" => "1",
            "memberid" => $id
        ));

        $penmodel = FycDirectory::model()->findAllByAttributes(array(
            "status" => "0",
            "memberid" => $id
        ));

        $delmodel = FycDirectory::model()->findAllByAttributes(array(
            "status" => "2",
            "memberid" => $id
        ));

        $cardmodel = FycCardSendItems::model()->findAllByAttributes(array(
            "touser_id" => $id
        ));

        $favmodel = FycFavourite::model()->findAllByAttributes(array(
            "memberid" => $id
        ));
        
        $scmodel = FycSavecardCategories::model()->findAllByAttributes(array(
            "memberid" => $id
        ));

        $body = array();

        $body['pub_count'] = count($publmodel);
        $body['pen_count'] = count($penmodel);
        $body['del_count'] = count($delmodel);
        $body['card_count'] = count($cardmodel);
        $body['fav_count'] = count($favmodel);
        $body['saved_card_count'] = count($scmodel);

        if (!empty($body))
        {
            $this->_sendResponse(200, $body);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetprofile()
    {

        $this->_authenticate();

        if (!isset($_GET['id']) || $_GET['id'] == 0) $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $model = FycMember::model();
        $cri = new CDbCriteria();

        $cri->condition = "memberid='" . $id . "'";
        $users = $model->getCommandBuilder()
            ->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users))
        {
            $this->_sendResponse(200, $users);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
    }

    public function actionreviewdumpdata()
    {
        $this->_authenticate();

        $rvold = FycReviewOld::model()->findAll();
        $count = 1;
        foreach ($rvold as $old)
        {
            $rv = new FycReview();

            $rv->memberid = $old['memberid'];
            $rv->directoryid = $old['directoryid'];
            $rv->title = $old['title'];
            $rv->comments = $old['review'];
            $rv->status = '1';
            $rv->rating = round($old['rating']);
            $rv->created = $old['date'];
            if ($rv->save()) echo "count====" . $count . "====<br>";
            else echo "error===" . var_dump($rv->errors);

            $count = $count + 1;

        }
    }

    public function actionmakedumpdata()
    {
        $this->_authenticate();

        $dirold = FycDirectoryOld::model()->findAll();
        $count = 1;
        foreach ($dirold as $old)
        {
            $dir = new FycDirectory();

            $dir->title = $old['directorytitle'];
            $dir->memberid = $old['memberid'];
            $dir->tagline = '';
            $dir->description = $old['directorydescription'];
            $dir->logo = '';
            $dir->cover = '';
            $dir->galleryid = 0;
            $dir->email = $old['directoryemail'];
            $dir->phone = $old['directorycontactnumber'];
            $dir->website = $old['directoryurl'];
            $dir->company_twitter = '';
            $dir->company_facebook = '';
            $dir->company_instagram = '';
            $dir->postcode = $old['directoryzipcode'];
            $dir->address = $old['directoryaddress'];
            $subcatmodel = FycSubcategory::model()->findByPk($old['subcategoryid']);
            if ($subcatmodel != null) $dir->categoryid = FycSubcategory::model()
                ->findByPk($old['subcategoryid'])->categoryid;
            else $dir->categoryid = 0;
            $dir->subcategoryid = $old['subcategoryid'];
            $dir->createddatetime = $old['createddatetime'];
            if ($dir->save()) echo "count====" . $count . "====<br>";
            else echo "error===" . var_dump($dir->errors);

            $count = $count + 1;

        }

    }
    
    public function actiondownloadpdf()
    {
        //$this->_authenticate();
        
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $card_id = $_GET['id'];
        
        $cardmodel = FycBusinesscards::model()->findByPk($card_id);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('c', 'A7-L');
        $mPDF1->WriteHTML($this->renderPartial('business-card', array('model' => $cardmodel), true));
        $mPDF1->Output("images/pdf/card-" . $card_id . ".pdf", 'F');
        
        $body = array();

        $body['pdf_url'] = Yii::app()->getBaseUrl(true) . "/images/pdf/card-" . $card_id . ".pdf";
        
        if (!empty($body))
        {
            $this->_sendResponse(200, $body);
        }
        else
        {
            $this->_sendResponse(507, array());
        }
        
    }

    

    public function actionbusinessdata()
    {
       $model = FycDirectory::model()->findAll();
       $count = 1;
       foreach ($model as $dir)
       {
           $cardmodel = new FycBusinesscards();
           
           $cardmodel->name = $dir['title'];
           $cardmodel->memberid = $dir['memberid'];
           $cardmodel->directory_id = $dir['id'];
           $cardmodel->email = $dir['email'];
           $cardmodel->phone = $dir['phone'];
           $cardmodel->address = $dir['address'];
           
           if ($cardmodel->save()) echo "count====" . $count . "====<br>";
            else echo "error===" . var_dump($cardmodel->errors);
            
             $count = $count + 1;
       }
    }
    /**
     * Notify the user
     */
    public function notifysentotp($email, $rendomnumber)
    {
        $body = "this is your otp number : " . $rendomnumber;
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.onemapweb.com';
        $mail->Username = 'info@onemapweb.com';
        $mail->Password = '#F4hpo91';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@onemapweb.com', 'Onemap');
        $mail->Subject = 'Your account details for Onemapweb.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->Send();

    }

    public function notifyenquiry($email, $name, $message)
    {
        $body = "Name : " . $name . "<br>";

        $body .= "message : " . $message;
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mangalaisai.com';
        $mail->Username = 'info@mangalaisai.com';
        $mail->Password = 'gz~MFZUsb*qS';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom($email);
        $mail->Subject = 'Enquiry';
        $mail->MsgHTML($body);
        $mail->AddAddress('ilayarajapms@gmail.com');
        $mail->Send();
    }

    private function _sendResponse($status = 200, $body = array() , $content_type = 'text/html')
    {

        // set the status
        // and the content type
        header('Content-type: ' . $content_type);

        // this should be templated in a real-world solution
        $arr = array();

        $arr['status'] = $status;

        $arr['message'] = $this->_getStatusCodeMessage($status);

        $arr['data'] = $body;

        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        Yii::app()->end();

    }

    private function _sendResponsewithMessage($status = 200, $message = '', $content_type = 'text/html')
    {
        // set the status
        // and the content type
        header('Content-type: ' . $content_type);

        // this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $message;
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {

        $codes = Array(

            200 => 'OK',

            400 => 'Bad Request',

            401 => 'Unauthorized',

            402 => 'Payment Re85red',

            403 => 'Forbidden',

            404 => 'Not Found',

            405 => 'Invalid Authorization Credentials',

            500 => 'Internal Server Error',

            501 => 'Not Implemented',

            502 => 'Email already Exists',

            503 => 'Invalid Email Error',

            504 => 'Invalid Email or Password',

            505 => 'Passenger Details Missing',

            506 => 'Car Model Not Found',

            507 => 'No Results Found',

            508 => 'Upload failed',

            508 => 'Upload failed',

            509 => 'Please complete your steps',

        );

        return (isset($codes[$status])) ? $codes[$status] : '';

    }

    private function _checkAuth()
    {

        // 		list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['REDIRECT_REDIRECT_HTTP_AUTHORIZATION'], 6)));
        // 			if (!isset($_SERVER['PHP_AUTH_USER']) && !isset($_SERVER['PHP_AUTH_PW'])) {
        // 				return false;
        // 			}
        // 			else {
        if ($_SERVER['PHP_AUTH_USER'] == 'onemap' && $_SERVER['PHP_AUTH_PW'] == 'onemap')
        {

            return true;

        }
        else

        return false;

        //}
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        // if(!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
        // 	// Error: Unauthorized
        // 	$this->_sendResponse(401);
        // }
        // $username = $_SERVER['HTTP_X_USERNAME'];
        // $password = $_SERVER['HTTP_X_PASSWORD'];
        // // Find the user
        // $user = Member::model()->find('LOWER(username)=?',array(strtolower($username)));
        // if($user===null) {
        // 	// Error: Unauthorized
        // 	$this->_sendResponse(401, 'Error: User Name is invalid');
        // } else if(!$user->validatePassword($password)) {
        // 	// Error: Unauthorized
        // 	$this->_sendResponse(401, 'Error: User Password is invalid');
        // }
        
    }

    /**
     * Returns the json or xml encoded array
     *
     * @param mixed $model
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */

    private function _getObjectEncoded($model, $array)
    {

        if (isset($_GET['format']))

        $this->format = $_GET['format'];

        if ($this->format == 'json')
        {

            return
;

        }
        elseif ($this->format == 'xml')
        {

            $result = '<?xml version="1.0">';

            $result .= "\n<$model>\n";

            foreach ($array as $key => $value)

            $result .= "    <$key>" . utf8_encode($value) . "</$key>\n";

            $result .= '</' . $model . '>';

            return $result;

        }
        else
        {

            return;

        }

    }

    public function _authenticate()
    {

        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']))
        {
            $header = explode(':', base64_decode(substr($_SERVER['REDIRECT_HTTP_AUTHORIZATION'], 6)));

            if (($header[0] == 'admin') && ($header[1] == 'onemap@123'))
            {

            }
            else
            {
                $this->_sendResponse(405, array());
            }
        }
        else
        {
            $this->_sendResponse(405, array());
        }

    }

    // }}}
    
}

?>
