<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$pagename = "Country";
	$redirecturl = "country-view.php";
	$tablename = "fyc_country";
	$idfield = "countryid";
	$namefield ="country";
	$statusfield = "countrystatus";
	
	$errormessage="";
	if (isset($_POST['cancel']) == true) { header("location: $redirecturl"); }
	
	if (isset($_POST['submit']) == true) 
	{
		
		$vnamefield = $_POST['namefield'];
		$vstatusfield = $_POST['statusfield'];

		if($vnamefield != ""){

		
		
		if(isset($_GET['id']) && $_GET['id'] != "") 
		{
			$query = "update $tablename set $namefield = '$vnamefield', $statusfield = '$vstatusfield', updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp() where ($idfield = ".$_GET['id'].")";
		}
		else 
		{
			$query = "insert into $tablename ($namefield, $statusfield, createdby, createddatetime) values ('$vnamefield', $vstatusfield, '$_SESSION[$loginid]', current_timestamp())";
		}
		
		$result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		if ($result == true) {
			if(isset($_GET['id']) && $_GET['id'] == "") {
				header("location: $redirecturl?mode=save");
			}
			else {
				header("location: $redirecturl?mode=update");
			}
		}
	} else {

	}		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>

<script language="javascript" type="text/javascript">
	function validate() {
	
		if( document.Form1.namefield.value == "" ) {
            alert( "Please provide Country name!" );
            document.Form1.namefield.focus() ;
            return false;
         }
		 return( true );
// 		DocumentForm.namefield.value = trim(DocumentForm.namefield);
// alert(DocumentForm.namefield.value);
	
		
		
	}
	
	function Cancel_ClickEvent() {
		window.location.href = "<?php echo $redirecturl; ?>";
	}
</script>
</head>

<body >

     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">

<h4>
<?php 
	echo $pagename." - ";
	if(isset($_GET['id']) && $_GET['id'] == "") { echo "Add New"; } else { echo "Modify"; }
?>
</h4><br><br>

<div id="admSndClnAdd">

<?PHP
    $vnamefield ="";
	$vstatusfield="";
	if(isset($_GET['id']) && $_GET['id'] != "" && $errormessage == "") {
		$query = "select $idfield, $namefield, $statusfield from $tablename where ($idfield = ".$_GET['id'].")";
		$equery = mysql_query($query) or die(mysql_error());
		$fetchrow = mysql_fetch_row($equery);
		$vnamefield = $fetchrow[1];
		$vstatusfield = $fetchrow[2];
	}
?>
<form id="Form1" name="Form1" action="<?PHP $PHP_SELF ?>" method="post" onsubmit = "return(validate());">

	<?PHP
		if ($errormessage != "") { echo "<tr><td colspan='2'>".$errormessage."</td></tr>"; }
	?>
	<div class="row">
	<div class="col-lg-5">
	<div class="form-group">
		<td><label for=""><?php echo $pagename; ?></label>
		<input type="text" id="namefield" name="namefield" value="<?PHP echo $vnamefield; ?>" class="form-control" /></td>
	</div></div>
	<div class="col-lg-5">
	<div class="form-group">
		<td><label for="Status">Status</label>
			<select id="statusfield" name="statusfield" class="form-control">
				<option <?PHP if($vstatusfield == "" || $vstatusfield == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($vstatusfield == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	</div></div></div>
	<tr>
		<td></td>
		<td >
			<input type="submit" id="submit" name="submit" value="Submit" class="btn btn-success" />
			<input type="reset" id="reset" name="reset" value="Reset" class="btn btn-danger">
			<input type="button" id="cancel" name="cancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	</tr>


</div>

</form>
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

