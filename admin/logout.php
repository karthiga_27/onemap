<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");

    if(isset($_SESSION[$loginid]))
    {
      session_destroy();
      header("Location:login.php?message=Successfully sign out");
    }
?>
