<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	$statusfield="";
	$categoryfield="";
	$categoryiconfield="";

	
	if(isset($_GET['id']) && $errormessage=="")
	{
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query = "select category, categorystatus, icon from fyc_category where categoryid = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
	        
	        if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
		        $categoryfield=$fetchrow['category'];
		        $statusfield=$fetchrow['categorystatus'];
                        $categoryiconfield=$fetchrow['icon'];
		    }
		    else
	 	    {
		        header("location: category-view.php?errormessage=Invalid category");
		    }
        }
		else
	    {
		    header("location: category-view.php?errormessage=Invalid category");
		}
	}
			
		
	if(isset($_POST['btnSubmit']) == true) 
	{
	    if($_POST['category']!="" && ($_POST['status']==1 || $_POST['status']==0 && $_POST['icon']!=""))
        {
                $_POST['category']=mysql_escape_string(htmlentities($_POST['category'])); 
		        $_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);
		        $_POST['icon']=mysql_escape_string(htmlentities($_POST['icon'])); 
			
				if(isset($_GET['id']))
			    {
			        $query = "update fyc_category set category = '$_POST[category]', categorystatus = '$_POST[status]', updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp(), icon = '$_POST[icon]' where  categoryid = '$_GET[id]'";
			    }
			    else
			    {
			        $query = "insert into fyc_category (category, categorystatus, createdby, createddatetime, icon) values ('$_POST[category]','$_POST[status]','$_SESSION[$loginid]',current_timestamp(),'$_POST[icon]')";
			    } 
			
			    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		        if($result == true) 
		        {
			        if(isset($_GET['id'])) 
			        {
				        header("location: category-view.php?message=update&forward=$_GET[forward]");
						exit();
			        }
			        else 
			        {
				        header("location: category-view.php?message=save&forward=$_GET[forward]");
			        }
		        }		
		        else
				{
				    header("location: category-view.php?errormessage=Fail to add or update category&forward=$_GET[forward]");
				}
		}
        else
        {
        $errormessage="Data incomplete";
        }		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function FormValidation(DocumentForm) 
	{
		DocumentForm.category.value = trim(DocumentForm.category);
        if(DocumentForm.category.value == "") 
		{
			alert("Enter category");
			DocumentForm.category.focus();
			return false;
		}
		return true;
	}
	
	function Cancel_ClickEvent() 
	{
		window.location.href = "category-view.php";
	}
</script>
</head>

<body>

<div id="wrapper">
     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>
		<?php 
	        
			echo "Category"." - ";
	        if(!isset($_GET['id'])) 
	        { echo "Add New"; } 
	        else 
	        { echo "Modify";  }
        ?>
		</h4><br><br>
		<div id="admSndClnAdd">
		
		<form name="dataform" action="<?PHP $PHP_SELF ?>" method="post" onSubmit="return FormValidation(this);">
	
		
		<?PHP
		if ($errormessage != "") { echo "<tr><td colspan='2'><h4>".$errormessage."</h4></td></tr>"; }
	    ?>
		
		<div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Category</label>
		<input type="text"  name="category" value="<?PHP echo $categoryfield; ?>" class="form-control" />
	   </div></div></div>
	    
		<div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Status</label>
			<select  name="status" class="form-control">
				<option <?PHP if($statusfield == "" || $statusfield == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($statusfield == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	     </div>
	 </div>
	</div>
         

    <!--<div class="row">
	    <div class="col-lg-5">
	       <div class="form-group"><label for="">Icon</label>
		       <input type="file"  name="icon" value="<?PHP echo $categoryiconfield; ?>" class="form-control" />     
	    </div>
	    </div>
    </div>-->

        <div class="row">
	    <div class="col-lg-5">
	    <div class="form-group">
		<td><label for="">Icon</label>
		<input type="text"  name="icon" value="<?PHP echo $categoryiconfield; ?>" class="form-control" />
                </td>
	    </div>
	     </div>
          </div>

	    <tr>
		<td></td>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		
	
		</form>
		</div>
<!-- sndCln End -->
 </div>
      </div>
 </div></div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>
