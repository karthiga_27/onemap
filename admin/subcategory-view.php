<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	if(isset($_GET['delid'])) {
	    $query="DELETE FROM fyc_subcategory WHERE subcategoryid = '$_GET[delid]'";
		$equery = mysql_query($query) or die(mysql_error());
	}
	
	if($_SESSION[$logincategory]==1 && isset($_GET['del']))
	{   
	    $_GET['del']=mysql_escape_string(htmlentities($_GET['del'])); 
		$_GET['del']=filter_var($_GET['del'], FILTER_VALIDATE_INT);
	    if($_GET['del']>=1)
	    {
     	        $query="select directoryid, directoryaudio from fyc_directory where subcategoryid='$_GET[del]'";
			    $equery = mysql_query($query) or die(mysql_error());	
			    
				if(mysql_num_rows($equery)>=1)
				{
				    while($fetch1 = mysql_fetch_array($equery))
				    {
			            if($fetch1['directoryaudio']!="")
					    {
					        $path="../upload/directory";
			                $path=$path.$_GET['id']."/audio/".$fetch1['directoryaudio'];
						    if(is_file($path))
						    {
						        unlink($path);
						    }
					    }
			            
						$query="select videoname from fyc_videogallery where directoryid='$fetch1[directoryid]'";
			            $equery = mysql_query($query) or die(mysql_error());
                        
                        if(mysql_num_rows($equery)>=1)
                        {
                            while($fetch2 = mysql_fetch_array($equery))
							{
							    if($fetch2['videoname']!="")
								{
								    $path="../upload/directory";
			                        $path=$path.$_GET['id']."/video/".$fetch2['videoname'];
						            if(is_file($path))
						            {
						                unlink($path);
						            }
								}
							}
						}
                        
                        $Query = "delete from fyc_videogallery where directoryid = '$fetch1[directoryid]'";
		                $EQuery = mysql_query($Query) or die(mysql_error());	


                        $query="select imagename from fyc_imagegallery where directoryid='$fetch1[directoryid]'";
			            $equery = mysql_query($query) or die(mysql_error());
                        
                        if(mysql_num_rows($equery)>=1)
                        {
                            while($fetch2 = mysql_fetch_array($equery))
							{
							    if($fetch2['imagename']!="")
								{
								    $path="../upload/directory";
			                        $path=$path.$_GET['id']."/image/".$fetch2['imagename'];
						            if(is_file($path))
						            {
						                unlink($path);
						            }
								}
							}
						}
                        
                        $Query = "delete from fyc_imagegallery where directoryid = '$fetch1[directoryid]'";
		                $EQuery = mysql_query($Query) or die(mysql_error());												
				        
				    }
				}
				
            $Query = "delete from fyc_directory where subcategoryid = '$_GET[del]'";
		    $EQuery = mysql_query($Query) or die(mysql_error());			
			
			$Query = "delete from fyc_subcategory where subcategoryid = '$_GET[del]'";
		    $EQuery = mysql_query($Query) or die(mysql_error());
			
			$message = "Record deleted successfully";
	    }
		else
		{
		    $errormessage="Invalid subcategory";
		}
	}
	
	$query="select categoryid,category from fyc_category where categorystatus=1 order by category asc";
	$cquery = mysql_query($query) or die(mysql_error());
    if(mysql_num_rows($cquery)==0)
	{
	    header("Location:category-view.php?errormessage=Create new category (or) Set category in active");
	    exit();
	}
	
	if(isset($_GET['errormessage'])) 
	{
		$errormessage = $_GET['errormessage'];
	}
	
	if(isset($_GET['message'])) 
	{
		$message = "Record ".$_GET['message']."d successfully";
	}
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
	function ChooseCategory()
	{
	window.location.href ="subcategory-view.php"+"?id="+document.selectcategory.cid.value;
	}
</script>
</head>

<body>

<!-- body Begin -->
  <div id="wrapper">
         <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
        <?php
		echo "<div class='container'><h4>Sub Category</h4></div>"; 
		echo "<div class='row'><div class='col-md-10 hidden-sm hidden-xs'>";
        echo "<a href='subcategory.php' title='Click here to add new subcategory' class='btn btn-info' role='button'style='float:right'>Add New</a>";
        echo "</div></div>";
		?>
               
               <div class="container">
               	<div class="row">
               		<div class="col-lg-5">
               				<div class="form-group">
			    <label for="Category">Category</label>
				    <form name="selectcategory">
	                <select name="cid" onchange="ChooseCategory()" class="form-control" >
	                <?php
					$first=0;
	                while($fetchcategory = mysql_fetch_array($cquery))
				    {
					    if(isset($_GET['id']) && $_GET['id']==$fetchcategory['categoryid'] && errormessage!="")
						{
						?>
	                    <option value="<?php  echo $fetchcategory['categoryid']; ?>" selected><?php echo $fetchcategory['category']; ?></option> 
	                    <?php
						}
						else if($first==0)
						{
						$first=$fetchcategory['categoryid'];
						?>
	                    <option value="<?php  echo $fetchcategory['categoryid']; ?>"><?php echo $fetchcategory['category']; ?></option> 
	                    <?php
						}
						else
						{
						?>
	                    <option value="<?php  echo $fetchcategory['categoryid']; ?>"><?php echo $fetchcategory['category']; ?></option> 
	                    <?php
						}
					}
					?>
					</select>
					</form>
				</div>
               		</div>
             	</div></div>
               
	 
        <?php
		
		$start=0;
		if(isset($_GET['forward']))
		{
		    $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
		    $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
	        
			if($_GET['forward']>=1)
			{
			    $start=$_GET['forward'];
		    }
			else
			{
			    $start=0;
			}
		}
		
		if(isset($_GET['id']) && $errormessage=="")
		{
		    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		    $_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	        if($_GET['id']>=1)
		    {
		        $query = "select subcategoryid, subcategory, subcategorystatus from fyc_subcategory where categoryid='$_GET[id]' order by subcategory asc limit $start, 11";
		    }
			else
			{
			    header("Location:subcategory-view.php?errormessage=Invalid category");
			}
		}
		else
		{
		$query = "select subcategoryid, subcategory, subcategorystatus from fyc_subcategory where categoryid=$first order by subcategory asc limit $start, 11";
	    }
		$equery = mysql_query($query);
		
		echo "<table>";
		if($message != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		echo "<thead><tr><th>Sl No</th><th>Subcategory</th><th>Status</th><th>Meta</th><th>E</th><th>D</th></tr></thead>";
		
		if(mysql_num_rows($equery)==0) 
	    {
		    echo "<tr><td colspan='7' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
	    }
		else
		{
		    $iRow = 1;
			while($iRow<=50 && $fetchrow = mysql_fetch_array($equery))
            {
                echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
			    echo "<td data-column='Sl No'>".$iRow."</td>";
			    echo "<td data-column='Subcategory'>".$fetchrow['subcategory']."</td>";
				
				echo "<td data-column='Stauts'>";
			    if($fetchrow['subcategorystatus'] == "1") { echo "Active"; } else { echo "InActive"; }
			    echo"</td>";
                            echo "<td data-column='M'><a href='#' onclick=\"showwindow('meta.php?from=subcategory&id=$fetchrow[subcategoryid]')\"><img src='images/meta.png' alt='Meta' title='Meta'></a></td>";         
				
				echo "<td data-column='E'><a href='subcategory.php?id=$fetchrow[subcategoryid]&forward=$start'><img src='images/edit.gif' alt='Edit' title='Edit'></a></td>";
            
			   
				echo "<td data-column='D'><a href='#' onclick=\"DeleteSubcategory($fetchrow[subcategoryid])\"><img style='width:20px;' src='images/remove.png' alt='Delete' title='Delete' /></a></td>";
				
			
			    echo "</tr></tbody>";
			    $iRow += 1;
			}			
		}
		echo "</table>";
		
		if($start>=20)
	    {
			$previous=$start-20;
			if(isset($_GET['id']))
		    {
			    echo "<a href='subcategory-view.php?id=$_GET[id]&forward=$previous'><img src='images/previous.png' /></a>";
			}
			else
		    {
			    echo "<a href='subcategory-view.php?id=$first&forward=$previous'><img src='images/previous.png' /></a>";
		    }
		}
	    echo "&nbsp;&nbsp;&nbsp;";
		if(mysql_num_rows($equery)==11)
		{
			$next=$start+20;
			if(isset($_GET['id']))
			{
			    echo "<a href='subcategory-view.php?id=$_GET[id]&forward=$next'><img src='images/forward.png' /></a>";
		    }
			else
			{
			    echo "<a href='subcategory-view.php?id=$first&forward=$next'><img src='images/forward.png' /></a>";
			}
		}
		
		?>
        </div>
      </div>
 </div>
 <script>
     function DeleteSubcategory(id) {
         if(confirm('Are You Sure to Delete?'))
           window.location.href="subcategory-view.php?delid="+id; 
     }
 </script>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>