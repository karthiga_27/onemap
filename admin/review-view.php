<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	if($_SESSION[$logincategory]==1 && isset($_GET['del']) && isset($_GET['id']))
	{   
	    $_GET['del']=mysql_escape_string(htmlentities($_GET['del'])); 
		$_GET['del']=filter_var($_GET['del'], FILTER_VALIDATE_INT);
		
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
		
	    if(($_GET['del']>=1) && ($_GET['id']>=1))
	    {
     	    $Query = "delete from fyc_review where reviewid = '$_GET[del]'";
		    $EQuery = mysql_query($Query) or die(mysql_error());				
		    if( $EQuery == true)
			{
			        $total="";
				    $query1="select rating from fyc_review where directoryid='$_GET[id]'";
		            $equery1 = mysql_query($query1) or die(mysql_error());		     
		            $found=mysql_num_rows($equery1);
					if($found>=1)
		            {
					    while($rate = mysql_fetch_array($equery1))
		                {
		                    $total=$total+$rate['rating'];
				
	 	                }
			            $total=$total/(mysql_num_rows($equery1));
					}
					else
					{
					    $total=0;
					}
				    $query2 = "update fyc_directory set rating = '$total' where directoryid=$_GET[id]";
					$equery2 = mysql_query($query2) or die(mysql_error());
			        $message = "Record deleted successfully";
			}
			else
			{
			    $errormessage="Invalid review";
	        } 
		}
		else
		{
		    $errormessage="Invalid review";
		}
	}
	
	if(isset($_GET['errormessage'])) 
	{
		$errormessage = $_GET['errormessage'];
	}
	
	if(isset($_GET['message'])) 
	{
		$message = "Record ".$_GET['message']."d successfully";
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
</head>

<body>

     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
		
		<h4>Review</h4>
		<?php
		
		$start=0;
		if(isset($_GET['forward']))
		{
		    $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
		    $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
	        
			if($_GET['forward']>=1)
			{
			    $start=$_GET['forward'];
		    }
			else
			{
			    $start=0;
			}
		}
		
		if(isset($_GET['id']) && $errormessage=="")
		{
		    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		    $_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	        if($_GET['id']>=1)
		    {
		        $query = "select  id, title, status, rating, created from fyc_review where  directoryid='$_GET[id]' order by id desc limit $start, 11";
		    }
			else
			{
			    header("Location:profile-view.php?errormessage=Invalid review");
			}
			// print_r($query);
		}
		else
		{
		    header("Location:profile-view.php?errormessage=Invalid review");   
	    }
		$equery = mysql_query($query)or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		// print_r($equery);
		
		echo "<table>";
		
		if($message != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		echo "<thead><tr><th>Sl No</th><th>Title</th><th>Status</th><th>Rating</th><th>Date</th><th>E</th>";
		
		
		if($_SESSION[$logincategory]==1)
	    {
	        echo "<th>D</th></tr></thead>";
	    }
		else
		{
		    echo "</tr>";
		}
		$count = mysql_num_rows($equery);
		if($count==0) 
	    {
		    echo "<tr><td colspan='7' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
	    }
		else
		{
		    $iRow = 1;
			while( $iRow<=10 && $fetchrow = mysql_fetch_array($equery))
            {
                echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
			    echo "<td data-column='Sl No'>".$iRow."</td>";
			    echo "<td data-column='Tittle'>".$fetchrow['title']."</td>";
				
				echo "<td data-column='Status'>";
			    if($fetchrow['status'] == "1") { echo "Active"; } else { echo "InActive"; }
			    echo"</td>";

				echo "<td data-column='Rating'>".$fetchrow['rating']."</td>";
				echo "<td data-column='Date'>".$fetchrow['created']."</td>";
				  
				echo "<td data-column='E'><a href='review.php?rid=$_GET[id]&id=$fetchrow[id]&forward=$start'><img src='../images/edit.gif' alt='Edit' title='Edit'></a></td>";
				//echo "<td><a href='fyc-category.php?id=$fetchrow[categoryid]&forward=$start'><img src='../images/edit.gif' alt='Edit' title='Edit'></a></td>";
            
			    if($_SESSION[$logincategory]==1)
	            {
			        echo "<td data-column='D'><a href='#' onclick=\"DeleteReview('review-view.php?id=$_GET[id]&del=$fetchrow[id]&forward=$start','$fetchrow[title]')\"><img src='../images/delete.jpg' alt='Delete' title='Delete' /></a></td>";
		        }
			
			    echo "</tr></tbody>";
			    $iRow += 1;
			}	
        }
		echo "</table>";
		
	
		
			if($start>=10)
			{
			$previous=$start-10;
			echo "<a href='review-view.php?forward=$previous'><img src='../images/previous.png' /></a>";
			}
			echo "&nbsp;&nbsp;&nbsp;";
			if(mysql_num_rows($equery)==11)
			{
			$next=$start+10;
			echo "<a href='review-view.php?forward=$next'><img src='../images/forward.png' /></a>";
			}
       		
		
		
		?>
        		
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

