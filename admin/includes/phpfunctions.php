<?php

	function rteSafe($strText) {
		//returns safe code for preloading in the RTE
		$tmpString = $strText;
		
		//convert all types of single quotes
		$tmpString = str_replace(chr(145), chr(39), $tmpString);
		$tmpString = str_replace(chr(146), chr(39), $tmpString);
		$tmpString = str_replace("'", "&#39;", $tmpString);
		
		//convert all types of double quotes
		$tmpString = str_replace(chr(147), chr(34), $tmpString);
		$tmpString = str_replace(chr(148), chr(34), $tmpString);
		//$tmpString = str_replace("\"", "\"", $tmpString);
		
		//replace carriage returns & line feeds
		$tmpString = str_replace(chr(10), " ", $tmpString);
		$tmpString = str_replace(chr(13), " ", $tmpString);
		
		return $tmpString;
	}

	function fillmonthsinselect($selectedvalue) {
		if($selectedvalue == "") { $currentmonth = date("F"); } else { $currentmonth = $selectedvalue; }

		if($currentmonth == "January" || $selectedvalue == "January") { echo "<option selected='selected' value='January'>January</option>"; } else { echo "<option value='January'>January</option>"; }
		if($currentmonth == "February") { echo "<option selected='selected' value='February'>February</option>"; } else { echo "<option value='February'>February</option>"; }
		if($currentmonth == "March") { echo "<option selected='selected' value='March'>March</option>"; } else { echo "<option value='March'>March</option>"; }
		if($currentmonth == "April") { echo "<option selected='selected' value='April'>April</option>"; } else { echo "<option value='April'>April</option>"; }
		if($currentmonth == "May") { echo "<option selected='selected' value='May'>May</option>"; } else { echo "<option value='May'>May</option>"; }
		if($currentmonth == "June") { echo "<option selected='selected' value='June'>June</option>"; } else { echo "<option value='June'>June</option>"; }
		if($currentmonth == "July") { echo "<option selected='selected' value='July'>July</option>"; } else { echo "<option value='July'>July</option>"; }
		if($currentmonth == "August") { echo "<option selected='selected' value='August'>August</option>"; } else { echo "<option value='August'>August</option>"; }
		if($currentmonth == "September") { echo "<option selected='selected' value='September'>September</option>"; } else { echo "<option value='September'>September</option>"; }
		if($currentmonth == "October") { echo "<option selected='selected' value='October'>October</option>"; } else { echo "<option value='October'>October</option>"; }
		if($currentmonth == "November") { echo "<option selected='selected' value='November'>November</option>"; } else { echo "<option value='November'>November</option>"; }
		if($currentmonth == "December") { echo "<option selected='selected' value='December'>December</option>"; } else { echo "<option value='December'>December</option>"; }
		
		/*
		echo "<option value='January'>January</option>";
		echo "<option value='February'>February</option>";
		echo "<option value='March'>March</option>";
		echo "<option value='April'>April</option>";
		echo "<option value='May'>May</option>";
		echo "<option value='June'>June</option>";
		echo "<option value='July'>July</option>";
		echo "<option value='August'>August</option>";
		echo "<option value='September'>September</option>";
		echo "<option value='October'>October</option>";
		echo "<option value='November'>November</option>";
		echo "<option value='December'>December</option>";
		*/
	}

	function fillyearsinselect($selectedvalue) {
		$startyear = 2001;
		$endyear = 2020;
		$currentyear = date("Y");
		
		for($iyear=$startyear; $iyear<=$endyear; $iyear++) {
			if($currentyear == $iyear || $selectedvalue == $iyear) { echo "<option selected='selected' value='$iyear'>$iyear</option>"; }
			else { echo "<option value='$iyear'>$iyear</option>"; }
		}
	}
?>