<?php

    function thumbnail($source,$destination,$width,$height)
	{	   
		   $sizes = getimagesize($source);
			
			$expected_max_width		=   $height;
            $expected_max_height	=   $height;
			
			
            $originalw	=	$sizes[0];
            $originalh	=	$sizes[1];
    
            if ($originalh<$expected_max_height) 
			{
                if ($originalw<$expected_max_width)
				{
                    $imgwidth	=	$originalw;
                    $imgheight	=	$originalh;
                } 
				else 
				{
                    $imgheight	=	($expected_max_width * $originalh)/ $originalw;
                    $imgwidth	=	$expected_max_width;
                }
            } 
			else 
			{
                $new_height		=	$expected_max_height;
                $new_width		=	($expected_max_height * $originalw)/ $originalh;
    
                if ($new_width>$expected_max_width) 
				{
                    $new_height	=	($expected_max_width * $expected_max_height) / $new_width;
                    $new_width	=	$expected_max_width;
                }
    
                $imgwidth	=	$new_width;
                $imgheight	=	$new_height;
            }
            $new_h	=	$imgheight;
            $new_w	=	$imgwidth;
			$dest = imagecreatetruecolor($new_w,$new_h);


            $im=ImageCreateFromJPEG($source); 
            $width=ImageSx($im);              
            $height=ImageSy($im);     

            $imageResized=imagecreatetruecolor($new_w,$new_h);
            $imageTmp=imagecreatefromjpeg($source);
            imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0,$new_w,$new_h, $width, $height);
            ImageJPEG($imageResized,$destination);
	}
?>