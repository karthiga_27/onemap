<?PHP
	include("includes/dbconnections.php");
	mysql_select_db($db_database, $db_connection) or die(mysql_error());

	session_start();
	if($_SESSION['loginid'] == "") { header("location: login.php");  }
	
	$From = $_GET['from'];
	if($From == "mainmenu") {
		$TableName = "mainmenu_meta";
		$Heading = "Main Menu";
	}
	elseif($From == "quicklinks") {
		$TableName = "main_meta";
		$Heading = "Sub Menu";
	}
	
	
	if (isset($_POST['btnSubmit']) == true) {
		if($_GET['mode'] == "") {
			$Query = "insert into ".$TableName." (".$From."title, ".$From."keywords, ".$From."description, ".$From."id) values ('".$_POST['MetaTitle']."', '".$_POST['MetaKeywords']."', '".$_POST['MetaDescription']."', ".$_GET['id'].")";
		}
		elseif($_GET['mode'] == "update") {
			$Query = "update ".$TableName." set ".$From."title = '".$_POST['MetaTitle']."', ".$From."keywords = '".$_POST['MetaKeywords']."', ".$From."description = '".$_POST['MetaDescription']."', ".$From."id = ".$_GET['id']." where (".$From."metaid = ".$_GET['metaid'].")";
		}		
	}
	
	if($_GET['mode'] == "delete") {
		$Query = "delete from ".$TableName." where (".$From."metaid = ".$_GET['metaid'].")";
	}
	
	if($Query != "") {
		$EQuery = mysql_query($Query);
		if($EQuery == true) {
			//$Message = "<font color='green'><b>Record ".$_GET['mode']."d successfully</b></font>";
			header("location: meta.php?from=".$From."&id=".$_GET['id']);
		}
		else {
			$Message = "<font color='red'><b>Record not saved</b></font>";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?PHP echo $HTMLTITLEPrefix; ?> :: Meta</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/JScripts.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	function Cancel_ClickEvent() {
		window.close();
	}
</script>
</head>

<body style="margin:10px;">

<div id="metaTag">

<form id="FormHome" name="FormName" action="<?PHP $PHP_SELF ?>" method="post">

<h1>Meta Tags - <?PHP echo $Heading; ?></h1>

<table border="0" cellpadding="0" cellspacing="2px" align="center">
		
	<?PHP
		$MetaTitle = "";
		$MetaKeywords = "";
		$MetaDescription = "";
			
		if($_GET['mode'] == "update") {			
			$Query = "select * from ".$TableName." where (".$From."metaid = ".$_GET['metaid'].")";
			$EQuery = mysql_query($Query);
			while($FQuery = mysql_fetch_row($EQuery)) {
				$MetaTitle = $FQuery[1];
				$MetaKeywords = $FQuery[2];
				$MetaDescription = $FQuery[3];						
			}
		}
	?>
	<tr>
		<td>Title</td>
		<td><input type="text" id="MetaTitle" name="MetaTitle" value="<?PHP echo $MetaTitle; ?>" style="width:400px" /></td>
	</tr>
	<tr>
		<td>Description</td>
		<td>
			<textarea id="MetaDescription" name="MetaDescription" rows="4" cols="50"><?PHP echo $MetaDescription; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>Keywords</td>
		<td>
			<textarea id="MetaKeywords" name="MetaKeywords" rows="4" cols="50"><?PHP echo $MetaKeywords; ?></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?PHP
				$ControlDisabled = "";
				if($_GET['mode'] != "update") {
					$Query = "select count(*) from ".$TableName." where (".$From."id = ".$_GET['id'].")";
					$EQuery = mysql_query($Query);
					$FQuery = mysql_fetch_row($EQuery);
					if($FQuery[0] > 0) { $ControlDisabled = "disabled=\"disabled\""; }
				}				
			?>
			<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" <?PHP echo $ControlDisabled; ?> />
			<input type="button" id="btnCancel" name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" />
		</td>
	</tr>
	<?PHP
		if ($Message != "") {
			echo "<tr><td colspan='2'>".$Message."</td></tr>";
		}
	?>
</table>

<div id="meta">

<?PHP
	echo "<table border='0' cellpadding='0' cellspacing='0'>";
	
	$Query = "select ".$From."metaid, ".$From."title, ".$From."keywords, ".$From."description, ".$From."id from ".$TableName." where (".$From."id = ".$_GET['id'].")";
	$EQuery = mysql_query($Query);	
	if(mysql_num_rows($EQuery) == 0) {
		echo "<tr><td colspan='5' align='center'><font color='red'><b>There are no records</b></font></td></tr>";
	}
	else {
		$iRow = 1;
		while($FQuery = mysql_fetch_row($EQuery)) {
			echo "<tr><th>Title</th><td width='500px'>".$FQuery[1]."</td></tr>";
			echo "<tr><th>Keywords</th><td>".$FQuery[2]."</td></tr>";
			echo "<tr><th>Description</td><td>".$FQuery[3]."</td></tr>";
			echo "<tr><th>Edit</th><td width='20px' align='center'><a href='meta.php?from=".$From."&id=".$_GET['id']."&metaid=".$FQuery[0]."&mode=update'><img src='../images/edit.jpg' title='Edit' alt='Edit'></td></tr>";
			echo "<tr><th>Delete</th><td width='20px' align='center'><a href='#' onclick=\"DeleteConfirmation('meta.php?from=".$From."&id=".$_GET['id']."&metaid=".$FQuery[0]."&mode=delete', '".$FQuery[1]."')\"><img src='../images/delete.jpg' title='Delete' alt='Delete'></a></td></tr>";
			$iRow += 1;
		}
	}	
	echo "</table>";
?>
</form>

</div>

</div>

</div>

</body>
</html>