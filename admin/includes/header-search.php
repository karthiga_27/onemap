<html>
<head>

<script language="javascript" type="text/javascript">

var xmlhttp;

function GetXmlHttpObject()
{
    if (window.XMLHttpRequest)
    {
        return new XMLHttpRequest();
    }

    if (window.ActiveXObject)
    {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
   
   return null;
}


function stateChanged()
{
    if(xmlhttp.readyState==4)
    {
       document.getElementById("searchassist").innerHTML=xmlhttp.responseText;
    }
}


function showHint(str)
{

    xmlhttp=GetXmlHttpObject();
    if (xmlhttp==null)
    {
      alert ("Your browser does not support XMLHTTP!");
      return;
    }
	
    var url="includes/ajax-search.php";
    url=url+"?s="+str;
    xmlhttp.onreadystatechange=stateChanged;
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
	
}

</script>


</head>

<body>
<!-- search Begin -->
<div id="search">

<form name="find" action="results.php" name="search" method="get">

<input type="text" autocomplete="off" name="searchkeyword" id="searchkeyword" value="" class="searchBdr" onkeyup="showHint(this.value)" />
<input type="image" src="default/btn-go.gif" />
<!--
<p><a href="advance_search.php">Use Advance Search</a></p>
-->
<div id="searchassist"></div>

</form>


</div>
</body>
<!-- search End -->

</div>
<!-- header End -->
