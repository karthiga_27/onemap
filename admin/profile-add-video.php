<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	if(isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
	    $_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
	        $query="select directoryid from fyc_directory where directoryid='$_GET[id]'";
	        $equery = mysql_query($query) or die(mysql_error());
	        if(mysql_num_rows($equery)<1)
	        {
	            header("location: profile-view.php?errormessage=Invalid profile to upload video");
		        exit();
	        }
		}
		else
	    {
	        header("location: profile-view.php?errormessage=Invalid profile to upload video");
		    exit();
	    }
	}
	else
	{
	    header("location: profile-view.php?errormessage=Profile not found");
		exit();
	}
	
	if(isset($_FILES))
	{
	    $found=0;
        if(count($_FILES)>=1)
	    {
	        for ($i=1;($i <= count($_FILES) && basename($_FILES[$i]["name"])!="");$i++)
            {	  
		        
		        if ($_FILES[$i]["error"] > 0)
                {
                    if($errormessage!="")
			        {
			            $errormessage=$errormessage." , ".$_FILES[$i]["name"];
			        }
			        else
			        {
			            $errormessage=$_FILES[$i]["name"];
			        }
                }
                else
                {
		            $found=1;
	                if($i==1)
					{
					$path="../upload";
					if(is_dir($path) == false){ mkdir($path, 0777, true); }
	
	                $path .= "/directory".$_GET['id'];
	                if(is_dir($path) == false){ mkdir($path, 0777, true); }
			
			        $path .= "/video";
	                if(is_dir($path) == false){ mkdir($path, 0777, true); }
					}
					
					
					$query="select videocount from fyc_directory where directoryid='$_GET[id]'";
	                $equery = mysql_query($query) or die(mysql_error());
 	                if(mysql_num_rows($equery)>=1)
	                {
					    $ext=pathinfo(basename($_FILES[$i]["name"]),PATHINFO_EXTENSION);
						$ext=strtolower($ext);
						$allowedExtensions = array("mpeg","avi","mpg","mp4");
			            $n=count($allowedExtensions);
			
			            for($m=0;$m<$n;$m++)
			            {
			                if($allowedExtensions[$m]==$ext)
				            {
				            break;
				            }
			            }
						
						if($m==$n)
						{
						    if($errormessage!="")
			                {
			                    $errormessage=$errormessage." , ".$_FILES[$i]["name"];
			                }
			                else
			                {
			                    $errormessage=$_FILES[$i]["name"];
			                }
						}
						else
						{
						    if($_FILES[$i]["size"]<=66035712)   //It allow file size should be under 62.9 MB 
							{	
								$data=mysql_fetch_assoc($equery);
						        $videoname='v'.($data['videocount']+1).".".$ext;
						
						        $path="../upload/directory".$_GET['id']."/video";
			                    $path=$path."/".$videoname;
					
					            if(move_uploaded_file($_FILES[$i]["tmp_name"], $path)==true)
			                    {
			                        $query= "insert into fyc_videogallery(directoryid, videoname, status, createdby, createddatetime) values('$_GET[id]', '$videoname',1,'$_SESSION[$loginid]', current_timestamp())";
			                        $equery = mysql_query($query) or die(mysql_error());
						
						            $count=$data['videocount']+1;
						            $query="update fyc_directory set videocount=$count where directoryid='$_GET[id]'";
		                            $equery = mysql_query($query) or die(mysql_error());
			                    } 
			                    else
			                    {
			                        if($errormessage!="")
			                        {
			                            $errormessage=$errormessage." , ".$_FILES[$i]["name"];
			                        }
			                        else
			                        {
			                            $errormessage=$_FILES[$i]["name"];
			                        }
			                    }
						    }
							else
			                {
			                    if($errormessage!="")
			                    {
			                        $errormessage=$errormessage." , ".$_FILES[$i]["name"];
			                    }
			                    else
			                    {
			                        $errormessage=$_FILES[$i]["name"];
			                    }
			                }
						}
				    }
					else
					{
					    if($errormessage!="")
			            {
			                $errormessage=$errormessage." , ".$_FILES[$i]["name"];
			            }
			            else
			            {
			                $errormessage=$_FILES[$i]["name"];
			            }
					}
					
			    }
		        
	        }
	        
			if($found==0)
			{
			    header("location: profile.php?id=$_GET[id]&errormessage=Zero video file upload");
			    exit();
			}
			
			if($errormessage=="")
	        { 
			    header("location: profile-video.php?id=$_GET[id]&message=Successfully video upload");
			    exit();
			}
	        else
	        { 
			    header("location: profile-video.php?id=$_GET[id]&errormessage=$errormessage are not upload");
                exit();
			}
	        
		}
        else
        {
		    header("location: profile.php?id=$_GET[id]&errormessage=Zero video file upload");
			exit();
        }		
	}
	else
	{
	    header("location: profile.php?id=$_GET[id]&errormessage=Video not set");
	    exit();
	}
	
	
?>