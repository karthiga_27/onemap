<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	$statusfield="";
	$advertisementfield="";
	
	if(isset($_GET['id']) && $errormessage=="")
	{
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query = "select advertisement, advertisementstatus from fyc_advertisement where advertisementid = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
	        
	        if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
				$advertisementfield=$fetchrow['advertisement'];
		        $statusfield=$fetchrow['advertisementstatus'];
		    }
		    else
	 	    {
		        header("location: advertising-view.php?errormessage=Invalid advertisement");
		    }
        }
		else
	    {
		    header("location: advertising-view.php?errormessage=Invalid advertisement");
		}
	}
			
		
	if(isset($_POST['btnSubmit']) == true) 
    {
	    $_POST['advertisement']=mysql_escape_string(htmlentities($_POST['advertisement'])); 
		$_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);
				
		if($_POST['advertisement']!="" && ($_POST['status']==1 || $_POST['status']==0))
        {
                if(isset($_GET['id']))
			    {
			        $query = "update fyc_advertisement set advertisement = '$_POST[advertisement]', advertisementstatus = '$_POST[status]', updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp() where  advertisementid = '$_GET[id]'";
			    }
			    else
			    {
			        $query = "insert into fyc_advertisement (advertisement, advertisementstatus, createdby, createddatetime) values ('$_POST[advertisement]','$_POST[status]','$_SESSION[$loginid]',current_timestamp())";
			    } 
			
			    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		        if($result == true) 
		        {
			        if(isset($_GET['id'])) 
			        {
				        header("location: advertising-view.php?message=update&forward=$_GET[forward]");
						exit();
			        }
			        else 
			        {
				        header("location: advertising-view.php?message=save&forward=$_GET[forward]");
			        }
		        }		
		        else
				{
				    header("location: advertising-view.php?errormessage=Fail to add or update advertisement&forward=$_GET[forward]");
				}
		}
        else
        {
        $errormessage="Data incomplete";
        }		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function Cancel_ClickEvent() 
	{
		window.location.href = "events-view.php";
	}
</script>
</head>

<body>

     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>
		<?php 
	        
			echo "Advertisement"." - ";
	        if(!isset($_GET['id'])) 
	        { echo "Add New"; } 
	        else 
	        { echo "Modify";  }
        ?>
		</h4><br><br>
		<div id="admSndClnAdd">
		
		<form name="dataform" action="<?PHP $PHP_SELF ?>" method="post" onSubmit="return FormValidation(this);">
	
		
		<?PHP
		if ($errormessage != "") { echo "<tr><td colspan='2'><h4>".$errormessage."</h4></td></tr>"; }
	    ?>
		
		 <div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">advertisement</label>
		<td><input type="text"  name="advertisement" value="<?PHP echo $advertisementfield; ?>" class="form-control" /></td>
	    </div></div></div>
	    
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Status</label>
		<td>
			<select  name="status" class="form-control">
				<option <?PHP if($statusfield == "" || $statusfield == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($statusfield == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	    </div></div></div>
	
	    <tr>
		<td></td>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		</form>
						
		
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

