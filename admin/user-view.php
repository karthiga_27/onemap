<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$pagename = "User";
	$redirecturl = "user.php";
	$currenturl = "user-view.php";
	
	$message="";
	
	if(isset($_GET['id']) && $_GET['id'] != "") {
		$query = "delete from fyc_member where (memberid = ".$_GET['memberid'].")";
		$equery = mysql_query($Query) or die(mysql_error());
		$message = "Record deleted successfully";
	}
	elseif(isset($_GET['mode']) && $_GET['mode'] != "") {
		$message = "Record ".$_GET['mode']."d successfully";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $projecttitle." :: ".$pagename; ?></title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/JScripts.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
	function onchangecountry(countrycontrol) {
		window.location.href = "<?php echo $currenturl; ?>?countryid="+countrycontrol.value;
	}
</script>
</head>

<body>

<!-- body Begin -->
<div id="body">

<div id="bodyBg">

<?php include('includes/admin-header.php') ?>

<!-- content Begin -->
<div id="content">

<!-- fstCln Begin -->
<div id="fstCln">

<?php include('includes/admin-quicklinks.php') ?>

</div>
<!-- fstCln End -->

<div></div>

<!-- sndCln Begin -->
<div id="admSndCln">

<h4><?php echo $pagename; ?></h4>

<?php 
	if(isset($_GET['countryid']) && $_GET['countryid'] != "") { $countryurl = "?countryid=".$_GET['countryid']; }
	echo "<h5><a href='$redirecturl' title='Click here to add new $pagename'>Add New</a></h5>";
?>

<table border='0' cellpadding='0' cellspacing='0' width='100%'>


	<?php
	    
		$query = "select fyc_member.memberid, fyc_member.firstname, fyc_member.lastname, fyc_member.photo, fyc_member.memberstatus, fyc_member.loginid, fyc_member.password, fyc_member.email, fyc_member.telephone, fyc_member.address, fyc_member.dob, fyc_member.newsletter 
			from fyc_member";
		$equery = mysql_query($query);
	
		if($message != "") {
			echo "<tr><td colspan='8'><b><font color='green'>".$message."</font></b></td></tr>";
		}
		echo "<tr><th width='30px'>Sl No</th><th>".$pagename."</th><th>Country</th><th>Status</th><th width='20px'>E</th><th width='20px'>D</th></tr>";
		if(mysql_num_rows($equery) == 0) {
			echo "<tr><td colspan='8' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
		}
		else {
			$irow = 1;
			while($fetchrow = mysql_fetch_array($equery)) {
				//echo "<tr onmouseover=\"this.className='onmouseovertr';\" onclick=\"window.location.href='$redirecturl?id=$fetchrow[0]';\" onmouseout=\"this.className='onmouseouttr'\" title='Click here to edit this record'>";
				echo "<tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
				echo "<td>".$irow."</td>";
				echo "<td>".$fetchrow['firstname']."</td>";
				echo "<td>".$fetchrow['lastname']."</td>";
				echo "<td>";
				if($fetchrow['memberstatus'] == "1") { echo "Active"; } else { echo "InActive"; }
				echo"</td>";
				echo "<td><a href='$redirecturl?memberid=".$fetchrow['memberid']."'><img src='../images/edit.gif' alt='Edit' title='Edit'></a></td>";
				echo "<td><a href='#' onclick=\"DeleteConfirmation('$currenturl?memberid=".$fetchrow['memberid']."')\"><img src='../images/delete.jpg' alt='Delete' title='Delete' /></a></td>";
				echo "</tr>";
				$irow += 1;
			}
		}
	?>
	</table>
	
</div>
<!-- sndCln End -->

</div>
<!-- content End -->

<div></div>

<?php include('includes/admin-footer.php') ?>

</div>

</div>
<!-- body End -->

</body>
</html>