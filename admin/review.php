<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	$titlefield="";
	$reviewfield="";
	$ratingfield="";
	$reviewstatus="";
	
	if(isset($_GET['id']) && isset($_GET['rid']) && $errormessage=="")
	{
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
		
		$_GET['rid']=mysql_escape_string(htmlentities($_GET['rid'])); 
		$_GET['rid']=filter_var($_GET['rid'], FILTER_VALIDATE_INT);
	    if( ($_GET['id']>=1) && ($_GET['rid']>=1) )
		{
		    $query = "select title, comments, rating, status from fyc_review where id = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
	        
	        if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
		        $titlefield=$fetchrow['title'];
		        $reviewfield=$fetchrow['comments'];
				$ratingfield=$fetchrow['rating'];
				$reviewstatus=$fetchrow['status'];
		    }
		    else
	 	    {
		        header("location: profile-view.php?errormessage=Invalid review");
		    }
        }
		else
	    {
		    header("location: profile-view.php?errormessage=Invalid review");
		}
	}
			
		
	if(isset($_POST['btnSubmit']) == true) 
	{
	    if($_POST['title']!="" && $_POST['review']!="" && ( $_POST['rating']>=1 && $_POST['rating']<=5 ) && ($_POST['status']==1 || $_POST['status']==0))
        {
                $_POST['title']=mysql_escape_string(htmlentities($_POST['title'])); 
				$_POST['review']=mysql_escape_string(htmlentities($_POST['review'])); 
		        
		        $_POST['rating']=filter_var($_POST['rating'], FILTER_VALIDATE_INT);
				$_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);
				
				echo $_GET['id']."--".$_GET['rid']."--".$_POST['rating'];
				
			
				if(isset($_GET['id']) && isset($_GET['rid']))
			    {
			        $query = "update fyc_review set title = '$_POST[title]', comments = '$_POST[review]', rating = '$_POST[rating]' ,status = '$_POST[status]' where  id = '$_GET[id]'";
			        $equery = mysql_query($query) or die(mysql_error());
					
				    echo "directory id:".$_GET['rid'];
					
					$total="";
				    $query1="select rating from fyc_review where directoryid='$_GET[rid]'";
		            $equery1 = mysql_query($query1) or die(mysql_error());		     
		            $found=mysql_num_rows($equery1);
					if($found>=1)
		            {
					    while($rate = mysql_fetch_array($equery1))
		                {
		                    $total=$total+$rate['rating'];
				
	 	                }
			            $total=$total/(mysql_num_rows($equery1));
					}
					else
					{
					    $total=0;
					}
					echo "Total:".$total;
					
				    $query2 = "update fyc_directory set rating = '$total' where id=$_GET[rid]";
					$equery2 = mysql_query($query2) or die(mysql_error());
				}
			  
			    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		        if($result == true) 
		        {
			        header("location: review-view.php?id=$_GET[rid]&message=update&forward=$_GET[forward]");
				    exit();
			    }		
		        else
				{
				    header("location: review-view.php?id=$_GET[rid]&errormessage=Fail to add or update category&forward=$_GET[forward]");
				}
		}
        else
        {
        $errormessage="Data incomplete";
        }		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function reviewdata() 
	{
		if(dataform.title.value == "") 
		{
			alert("Enter Title");
			dataform.title.focus();
			return false;
		}
		return true;
	}
	
	function Cancel_ClickEvent() 
	{
		//window.location.href = "profile-view.php";
		window.location.href =  "review-view.php?id=<?php echo $_GET['rid']; ?>";
	}
</script>
</head>


<body>

     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>
		<?php 
	        
			echo "Review"." - ";
	        if(isset($_GET['id'])) 
	        { echo "Modify";  }
        ?>
		</h4><br><br>
		
		<form name="reviewdata" action="<?PHP $PHP_SELF ?>" method="post" onsubmit="return check()">
		
		<?PHP
		if ($errormessage != "") { echo "<tr><td colspan='2'><h4>".$errormessage."</h4></td></tr>"; }
	    ?>
		
		 <div class="row">
					<div class="col-lg-5">
					<div class="form-group">
						<label for="">Title</label>
		<input type="text"  name="title" value="<?PHP echo $titlefield; ?>" class="form-control" />
	    </div></div></div>
	    
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group">
						<label for="">Review</label>
		<textarea name="review" class="form-control"><?PHP echo $reviewfield; ?></textarea>
		</div></div></div>
		
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group">
						<label for="">Rating</label>
		<select name="rating" class="form-control">
		<?php
		    for($i=1;$i<=5;$i=$i+1)
			{
			    if($ratingfield==$i)
				{
				    echo "<option value='$i' selected>$i</option>";
				}
				else
				{			
		            echo "<option value='$i'>$i</option>";
				}
		    }
		?>
		</select>
		</div></div>
		<div class="col-lg-5">
					<div class="form-group">
						<label for="">Status</label>
		                    <select  name="status" class="form-control">
								<option <?PHP if($reviewstatus == "" || $reviewstatus == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
								<option <?PHP if($reviewstatus == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
							</select>
		                
	                </div>
	    </div></div>

	    <tr>
		<td></td>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		
		</form>
				
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

