<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	if(isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query="select imagename from fyc_imagegallery where directoryid='$_GET[id]'";
	        $equery = mysql_query($query) or die(mysql_error());
		}
		else
		{
		    header("location: profile-view.php?errormessage=Invalid profile to upload image");
			exit();
		}
	}
	else
	{
	    header("location: profile-view.php?errormessage=Profile not found");
		exit();
	}
	
	if(isset($_GET['message']))
	{
	    $message=$_GET['message'];
	}
	
	if(isset($_GET['errormessage']))
	{
	    $errormessage=$_GET['errormessage'];
	}
	

	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" src="../js/JScripts.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    function addElement() 
    {
        var count = '';
        if( typeof addElement.counter == 'undefined' ) 
 	    { 
            addElement.counter = 1;
        }
        ++addElement.counter;
	    count=addElement.counter;
	
	    var divIdName = count;
	    var base = document.getElementById('myDiv');
        var newdiv = document.createElement('div');
        newdiv.setAttribute('id',divIdName);
    
	    newdiv.innerHTML = "<tr><td><input type='file' "+"  name='"+count+"'/>"+"  "+"<a href='#' onclick='removeElement("+count+")'>Cancel</a></td></tr>";
        base.appendChild(newdiv);
    }

    function removeElement(count)
    {
        var base = document.getElementById('myDiv');
        var olddiv = document.getElementById(count);
        base.removeChild(olddiv);
    }
	
	function Cancel_ClickEvent() 
	{
		window.location.href = "profile.php?id=<?php echo $_GET['id']; ?>";
	}
	
	function Audio_ClickEvent(next) 
	{
	    window.location.href = "profile-audio.php?id="+next;
	}
	
	function Video_ClickEvent(next) 
	{
	   window.location.href = "profile-video.php?id="+next;
	}
	
</script>
</head>
<body>


     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
            	<h4>
		<?php 
	        
		    echo "Image Gallery";
	    
		?>
		</h4><br><br>
		<div id="admSndClnAdd">
		
		<tr>
		
		<td valign="top">
		<form name="dataform" action="profiles-add-image.php?id=<?php echo $_GET['id']; ?>" method="post" ENCTYPE="multipart/form-data">
		
		<?php
		if($message != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		?>
		
		
	
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Add Logo Image</label>
						<!-- <input type="file" name="logo" class="form-control"/> -->
						<input type="file" class="form-control" id="logo" name="logo" onchange="readURLs(this);" />
  <img class="basic-img" src="#" id='blahs'/>
  <div class="row" id="image_previews"></div>
					</div></div>
		</div>
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Add Cover Image</label>
						<!-- <input type="file" name="cover" class="form-control"/> -->
						<input type="file" class="form-control" id="cover" name="cover" onchange="readURL(this);" />
  <!-- <img class="basics-img" src="#" id='blah'/> -->
  <div class="row" id="image_preview"></div>
					</div></div>
		</div>
		
		<div id="myDiv"></div> 
	
	   
		
		<tr>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		
		</form>
		
	<!-- 	
		<div>

		<?php
			if($_GET['id']>=1)
			{
			    $query="select imagename from fyc_imagegallery where directoryid='$_GET[id]'";
		        $equery = mysql_query($query) or die(mysql_error());
			while($fetchrow = mysql_fetch_assoc($equery)){
				echo $fetchrow['imagename']; 
		?>

		<div>
			<img src="<?php echo $fetchrow['imagename']; ?>">
		</div>
			<?php } } ?>
		</div> -->


		</td>
		                           
		<td valign="top">
		<?php
		
		$start=0;
        if(isset($_GET['forward']))
        {
            $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
            $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
	        
            if($_GET['forward']>=1)
            {
                $start=$_GET['forward'];
            }
            else
            {
                $start=0;
            }
        }
		
		$query = "select  imagegalleryid, imagename, status from fyc_imagegallery where directoryid='$_GET[id]' order by imagegalleryid desc  limit $start, 6";
		$equery = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($equery)>=1)
		{
		    ?>
		
			<?php
			
			if($start>=5)
            {
                $previous=$start-5;
                echo "<a href='profiles-image.php?id=$_GET[id]&forward=$previous'><img src='../images/previous.png' /></a>";
            }

            echo "&nbsp;&nbsp;&nbsp;";
			
            if(mysql_num_rows($equery)==6)
            {
                $next=$start+5;
                echo "<a href='profiles-image.php?id=$_GET[id]&forward=$next'><img src='../images/forward.png' /></a>";
            }
	    }
		?>
		</td>
		
		</tr>
		
				
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 <script>
//  $(function() {
//     $(":file").change(function() {
//         if (this.files && this.files[0]) {
//             var reader = new FileReader();
//             reader.onload = imageIsLoaded;
//             reader.readAsDataURL(this.files[0]);
//         }
//     });
// });
// function imageIsLoaded(e) {
//     $('.basic-img').attr('src', e.target.result);
// };
 </script>
 <script type="text/javascript">
    function readURL(input) {
		var total_file=document.getElementById("cover").files.length;
		for(var i=0;i<total_file;i++)
 {
		$('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
        // if (input.files && input.files[0]) {
        //     var readers = new FileReader();

        //     readers.onload = function (e) {
        //         $('#blah').attr('src', e.target.result);
        //     }

        //     readers.readAsDataURL(input.files[0]);
        // }
 }
    }
	function readURLs(input) {
		var total_file=document.getElementById("logo").files.length;
		for(var i=0;i<total_file;i++)
 {
		$('#image_previews').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
        // if (input.files && input.files[0]) {
        //     var reader = new FileReader();

        //     reader.onload = function (e) {
        //         $('#blahs').attr('src', e.target.result);
        //     }

        //     reader.readAsDataURL(input.files[0]);
        // }
    }
	}
</script>
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>