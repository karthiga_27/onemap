<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	
	$errormessage="";
	if(isset($_GET['access']) && $_GET['access'] == "denied") { $errormessage = "Session expired, please login again.<br>Please check that cookies are enabled in your web browser."; }

if(isset($_POST['btnLogin'])) 
{
    
	$_POST['txtLoginId']=mysql_escape_string(htmlentities($_POST['txtLoginId'])); 
	$query = "SELECT id, loginid, loginpassword, loginname, logincategory, currentlogintime from fyc_adminlogin where loginid = '$_POST[txtLoginId]' and loginstatus = 1";
    $equery = mysql_query($query) or die (mysql_error());
	if(mysql_num_rows($equery) > 0) 
	{
	    $_POST['txtPassword']=mysql_escape_string(htmlentities($_POST['txtPassword'])); 
		$fetchrow = mysql_fetch_array($equery) or die(mysql_query());
		if ($fetchrow['loginpassword'] == md5($_POST['txtPassword'])) 
		{
		    $db_loginid = $fetchrow['id'];
			$db_previouslogintime = $fetchrow['currentlogintime'];
				
			$query = "update fyc_adminlogin set previouslogintime = '$db_previouslogintime' , currentlogintime = current_timestamp() where id = $db_loginid";
			$equery = mysql_query($query) or die (mysql_error());
				
			$_SESSION[$loginid] = $fetchrow['id'];
			$_SESSION[$loginname] = $fetchrow['loginid'];
			$_SESSION[$logincategory] = $fetchrow['logincategory'];
			
			header("location: index.php");
		}
		else 
		{ 
		    $errormessage = "Invalid Password"; 
		}
    }
	else 
	{ 
	    $errormessage = "Invalid Login Id"; 
	}
}

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	 <?PHP include('includes/admin-meta.php') ?>
	<script language="javascript" type="text/javascript">
	function FocusToLoginId() 
	{ 
	    document.formlogin.txtLoginId.focus(); 
    }

	function ValidateControls() 
	{
		if(document.formlogin.txtLoginId.value == "") 
		{
			alert("Enter Login Id");
			document.formlogin.txtLoginId.focus();
			return false;
		}
		
		if(document.formlogin.txtPassword.value == "") 
		{
			alert("Enter Password");
			document.formlogin.txtPassword.focus();
			return false;
		}
		return true;
	}
	</script>
	
</head>

<body onload="FocusToLoginId()" style="background-color:#52556F">
   <!--login modal-->
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog"><br><br><br><br><br><br>
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">Login</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block" name="formlogin" method="post" action="login.php" onsubmit="return ValidateControls();">
            <div class="form-group">
              <input name="txtLoginId" id="txtLoginId" class="form-control input-lg" placeholder="Login">
            </div>
            <div class="form-group">
              <input type="password" name="txtPassword" class="form-control input-lg" placeholder="Password">
            </div>
            <div class="form-group">
              <span class="pull-right"><a href="#">
              	<?PHP
				if (isset($_GET['logout']) && $_GET['logout'] == "true") 
				{         
				    echo("<font color='#FEFEFF'>You are now logged out.</font>"); 
				}
				if ($errormessage != "")  
				{      
				    echo("<font color='red'>".$errormessage."</font>"); 
				}
			    ?>
		    </a></span>
            </div>
         
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button class="btn btn-success" data-dismiss="modal" aria-hidden="true" name="btnLogin" id="btnLogin" value="Login">Login</button>
		  </div>	
      </div>
       </form>
  </div>
  </div>
</div></div>
</body>
</html>

