<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$pagename = "City";
	$redirecturl = "city-view.php";
	
	$errormessage="";

	if (isset($_POST['cancel']) == true) { header("location: $redirecturl"); }
	
	if (isset($_POST['submit']) == true) {
		$city = $_POST['city'];
		$countryid = $_POST['country'];
		$citystatus = $_POST['citystatus'];
		if($city != "" && $countryid != ""){
		if(isset($_GET['cityid']) && $_GET['cityid'] != "") 
		{
			$query = "update fyc_city set city = '$city', countryid = '$countryid', citystatus = '$citystatus', updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp() where (cityid = ".$_GET['cityid'].")";
		}	
		else 
		{
			$query = "insert into fyc_city (city, countryid, citystatus, createdby, createddatetime) values ('$city', '$countryid', '$citystatus', '$_SESSION[$loginid]', current_timestamp())";
		}
		
	    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		if ($result == true) {
			if(isset($_GET['countryid']) && $_GET['countryid'] != "") { $countryurl = "&countryid=".$_GET['countryid']; }
			if(isset($_GET['id']) && $_GET['id'] == "") {
				header("location: $redirecturl?mode=save".$countryurl);
			}
			else {
				header("location: $redirecturl?mode=update".$countryurl);
			}
		}	} else {

		}	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>

<script language="javascript" type="text/javascript">
	function FormValidation(DocumentForm) {
		DocumentForm.city.value = trim(DocumentForm.city);

		if(DocumentForm.city.value == "") {
			alert("Enter City");
			DocumentForm.city.focus();
			return false;
		}
		
		return true;
	}
	function validate() {
	
	if( document.Form1.city.value == "" ) {
		alert( "Please provide city name!" );
		document.Form1.city.focus() ;
		return false;
	 }
	 if( document.Form1.country.value == "" ) {
		alert( "Please Select country name!" );
		document.Form1.country.focus() ;
		return false;
	 }
	 return( true );
// 		DocumentForm.namefield.value = trim(DocumentForm.namefield);
// alert(DocumentForm.namefield.value);

	
	
}
	
	function Cancel_ClickEvent() {
		window.location.href = "<?php echo $redirecturl; ?>";
	}
</script>
</head>

<body onload="javascript:document.getElementById('city').focus();">


     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">


<h4>
<?php 
	echo $pagename." - ";
	if(isset($_GET['id']) && $_GET['id'] == "") { echo "Add New"; } else { echo "Modify"; }
?>
</h4><br><br>

<div id="admSndClnAdd">

<?PHP
	$city="";
	$countryid="";
	$citystatus="";
	
	if(isset($_GET['cityid']) && $_GET['cityid'] != "" && $errormessage == "") {
		$query = "select cityid, city, countryid, citystatus from fyc_city where (cityid = ".$_GET['cityid'].")";
		$equery = mysql_query($query) or die(mysql_error());
		$fetchrow = mysql_fetch_array($equery);
		$city = $fetchrow['city'];
		$countryid = $fetchrow['countryid'];
		$citystatus = $fetchrow['citystatus'];
	}
?>
<form id="Form1" name="Form1" action="<?PHP $PHP_SELF ?>" method="post" onsubmit = "return(validate());">

	<?php
		if ($errormessage != "") { echo "<tr><td colspan='2'>".$errormessage."</td></tr>"; }
	?>
	<div class="row">
	<div class="col-lg-5">
	<div class="form-group">
		<td><label for=""><?php echo $pagename; ?></label>
		<input type="text" id="city" name="city" value="<?PHP echo $city; ?>" class="form-control" /></td>
	</div></div></div>
	<div class="row">
	<div class="col-lg-5">
	<div class="form-group">
		<td><label for="">Country</label>
			<select id="country" name="country" class="form-control" onchange="onchangecountry(this)">
				<option value="">--- Select Country ---</option>
				<?php
					if($countryid == "") { $countryid = $_GET['countryid']; }
					$query = "select countryid, country from fyc_country order by country asc";
					$equery = mysql_query($query) or die(mysql_error());
					while($fetchrow = mysql_fetch_array($equery)) {
						if($fetchrow['countryid'] == $countryid) {
							echo "<option selected value='".$fetchrow['countryid']."'>".$fetchrow['country']."</option>";
						}
						else {
							echo "<option value='".$fetchrow['countryid']."'>".$fetchrow['country']."</option>";
						}
					}
				?>
			</select>
		</td>
	</div></div></div>

	<div class="row">
	<div class="col-lg-5">
	<div class="form-group">
		<td><label for="">Status</label>
			<select id="citystatus" name="citystatus" class="form-control">
				<option <?PHP if($citystatus == "" || $citystatus == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($citystatus == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	</div></div></div>
	<tr>
		<td></td>
		<td>
			<input type="submit" id="submit" name="submit" value="Submit" class="btn btn-success" />
			<input type="reset" id="reset" name="reset" value="Reset" class="btn btn-danger">
			<input type="button" id="cancel" name="cancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	</tr>


</form>

</div>

</form>
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>
