<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	$titlefield="";
	$statusfield="";
	$about="";
	$addressfield="";
	$landmarkfield="";
	$zipcodefield="";
	$cityfield="";
	$urlfield="";
	$emailfield="";
	$namefield="";
	$numberfield="";
	$cost="";
	$premium_label = "";
	$verify_label = "";
	$issupplier = "";
		
	if(isset($_GET['id']))
	{
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query = "select  directoryid,issupplier, verify_label, premium_label, subcategoryid, directorytitle, directorystatus, directorydescription, directoryaddress, directorylandmark,  directorycontactname,  directorycontactnumber, cityid, directoryzipcode,  directoryurl,  directoryemail, cost from fyc_directory where directoryid = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
				
		    if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
			    $directoryfield=$fetchrow['directoryid'];
			    $subcategoryidfield=$fetchrow['subcategoryid'];
		        $titlefield=$fetchrow['directorytitle'];
		        $statusfield=$fetchrow['directorystatus'];
			    $about=$fetchrow['directorydescription'];
			    $addressfield=$fetchrow['directoryaddress'];
			    $landmarkfield=$fetchrow['directorylandmark'];
			    $zipcodefield=$fetchrow['directoryzipcode'];
			    $cityfield=$fetchrow['cityid'];
			    $urlfield=$fetchrow['directoryurl'];
			    $emailfield=$fetchrow['directoryemail'];
			    $namefield=$fetchrow['directorycontactname'];
			    $numberfield=$fetchrow['directorycontactnumber'];
				$cost=$fetchrow['cost'];
				$verify_label=$fetchrow['verify_label'];
				$premium_label=$fetchrow['premium_label'];
				$issupplier=$fetchrow['issupplier'];
		    }
		    else
	 	    {
		        header("location: profile-view.php?errormessage=Invalid profile");
		    }
		}
		else
	    {
		    header("location: profile-view.php?errormessage=Invalid profile");
			exit();
		}
	}
	
	if(isset($_GET['sid']))
	{
	    $_GET['sid']=mysql_escape_string(htmlentities($_GET['sid'])); 
		$_GET['sid']=filter_var($_GET['sid'], FILTER_VALIDATE_INT);
	    if($_GET['sid']>=1)
		{
		    $query = "select  subcategoryid, categoryid from fyc_subcategory where subcategoryid = '$_GET[sid]'";
		    $equery = mysql_query($query) or die(mysql_error());
			if(mysql_num_rows($equery)>=1)
			{
			    $categorydata=mysql_fetch_assoc($equery);
			}
			else
			{
			    header("location: profile-view.php?errormessage=Invalid profile");
	            exit();
			}
		}
		else
	    {
	        header("location: profile-view.php?errormessage=Invalid profile");
	        exit();
	    }
	}
	
		
	if (isset($_POST['btnSubmit']) == true) 
	{
	    $_POST['subcategory']=mysql_escape_string(htmlentities($_POST['subcategory'])); 
		$_POST['subcategory']=filter_var($_POST['subcategory'], FILTER_VALIDATE_INT);
		
		$_POST['title']=mysql_escape_string(htmlentities($_POST['title'])); 
		
		$_POST['status']=mysql_escape_string(htmlentities($_POST['status']));
        $_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);		
		
	    $_POST['description']=mysql_escape_string(htmlentities($_POST['description'])); 
		$_POST['address']=mysql_escape_string(htmlentities($_POST['address'])); 
		$_POST['landmark']=mysql_escape_string(htmlentities($_POST['landmark'])); 
		
		$_POST['zipcode']=mysql_escape_string(htmlentities($_POST['zipcode'])); 
		//$_POST['zipcode']=filter_var($_POST['zipcode'], FILTER_VALIDATE_INT);
		
		$_POST['city']=mysql_escape_string(htmlentities($_POST['city']));
		$_POST['city']=filter_var($_POST['city'], FILTER_VALIDATE_INT);
		
		$_POST['url']=mysql_escape_string(htmlentities($_POST['url']));
		$_POST['email']=mysql_escape_string(htmlentities($_POST['email']));
		$_POST['contactname']=mysql_escape_string(htmlentities($_POST['contactname']));
		$_POST['contactnumber']=mysql_escape_string(htmlentities($_POST['contactnumber']));
		$_POST['verify_label']=mysql_escape_string(htmlentities($_POST['verify_label']));
		$_POST['premium_label']=mysql_escape_string(htmlentities($_POST['premium_label']));
		$_POST['issupplier']=mysql_escape_string(htmlentities($_POST['issupplier']));

		
		// if($_POST['subcategory']!='' && $_POST['subcategory']!='select' && $_POST['title']!='' && ($_POST['status']=='1' || $_POST['status']=='0') && $_POST['description']!='' && $_POST['address']!='' && $_POST['landmark']!='' && $_POST['zipcode']!='' && $_POST['city']!='' && $_POST['url']!='' && $_POST['email']!='' && $_POST['contactname']!='' && $_POST['contactnumber']!='')
		
	    if(isset($_GET['id']))
		{
		$query = "update fyc_directory set subcategoryid = '$_POST[subcategory]',  premium_label = '$_POST[premium_label]',  verify_label='$_POST[verify_label]', directorytitle = '$_POST[title]',  directorystatus='$_POST[status]', directorydescription = '$_POST[description]',  directoryaddress = '$_POST[address]', directorylandmark = '$_POST[landmark]', directoryzipcode = '$_POST[zipcode]',  cityid = '$_POST[city]',  directoryurl = '$_POST[url]',  directoryemail = '$_POST[email]',  directorycontactname = '$_POST[contactname]',  directorycontactnumber = '$_POST[contactnumber]',  updatedby = '$_SESSION[$loginid]',  updateddatetime = current_timestamp(),issupplier = '$_POST[issupplier]' where directoryid = '$_GET[id]'";
		}
		else
		{
		$query = "insert into fyc_directory(subcategoryid, directorytitle, directorystatus, verify_label, premium_label, directorydescription, directoryaddress, directorylandmark, directoryzipcode, cityid, directoryurl, directoryemail, directorycontactname, directorycontactnumber, createdby, createddatetime,issupplier) values ('$_POST[subcategory]','$_POST[title]','$_POST[status]','$_POST[verify_label]','$_POST[premium_label]','$_POST[description]', '$_POST[address]','$_POST[landmark]','$_POST[zipcode]','$_POST[city]','$_POST[url]','$_POST[email]','$_POST[contactname]','$_POST[contactnumber]','$_SESSION[$loginid]', current_timestamp(),'$_POST[issupplier]')";
		}
		
		$result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		if ($result == true) 
		{
		    if(isset($_GET['id'])) 
			{
			    header("location: profile.php?id=$_GET[id]&message=Successfully directory updated");
		    }
			else 
			{
			    $data=mysql_insert_id();
			    header("location: profile.php?id=$data&message=Sucessfully directory created");
		    }
	    }
		else
		{
		    header("location: profile-view.php?sid=$_POST[subcategory]&errormessage=Fail to add or update directory");
		}
		
    }
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>

<script language="javascript" type="text/javascript">
function Cancel_ClickEvent() 
{
	window.location.href = "profile-view.php";
}
	
function check_sheet()
{
    
	if(document.product.category.value=="select")
    {
        alert("Error: Choose category");
		document.product.category.focus();
		return false;
    }
	
	if(document.product.subcategory.value=="select")
    {
        alert("Error: Choose subcategory");
		document.product.subcategory.focus();
		return false;
    }
	
	if(document.product.title.value=="")
    {
        alert("Error: Title can't be blank");
		document.product.title.focus();
		return false;
    }
	
	// if(document.product.description.value=="")
 //    {
 //        alert("Error: Description can't be blank");
	// 	document.product.description.focus();
	// 	return false;
 //    }
	
	if(document.product.address.value=="")
    {
        alert("Error: Address can't be blank");
		document.product.address.focus();
		return false;
    }
	
	// if(document.product.landmark.value=="")
 //    {
 //        alert("Error: Landmark can't be blank");
	// 	document.product.landmark.focus();
	// 	return false;
 //    }
	
	// if(document.product.zipcode.value=="")
 //    {
 //        alert("Error: Zipcode can't be blank");
	// 	document.product.zipcode.focus();
	// 	return false;
 //    }
	
	
	// if(document.product.country.value=="select")
 //    {
 //        alert("Error: Choose country");
	// 	document.product.country.focus();
	// 	return false;
 //    }
	
	// if(document.product.city.value=="select")
 //    {
 //        alert("Error: Choose city");
	// 	document.product.city.focus();
	// 	return false;
 //    }
	
	
	// if(document.product.url.value=="")
 //    {
 //        alert("Error: URL can't be blank");
	// 	document.product.url.focus();
	// 	return false;
 //    }
	
    var url=/^(http)(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;=:%\$#_]*)?/;
    if(!url.test(document.product.url.value))
    {
        alert("Error: Invalid URL");
		document.product.url.focus();
        return false;
    }
	
	
	// if(document.product.email.value=="")
 //    {
 //        alert("Error: Email can't be blank");
	// 	document.product.email.focus();
	// 	return false;
 //    }
	
	var email=/^[_a-z 0-9-]+(\.[_a-z 0-9-]+)*@[a-z 0-9-]+(\.[a-z 0-9-]+)*(\.[a-z]{2,3})$/;
	if(!email.test(document.product.email.value))
	{
	    alert("Error: Invalid email ID");
		document.product.email.focus();
		return false;
	}
	
	// if(document.product.contactname.value=="")
 //    {
 //        alert("Error: Contact name can't be blank");
	// 	document.product.contactname.focus();
	// 	return false;
 //    }
	
	// if(document.product.contactnumber.value=="")
 //    {
 //        alert("Error: Contact number can't be blank");
	// 	document.product.contactnumber.focus();
	// 	return false;
 //    }
	
	return true;
	
}
</script>

<script language="javascript" type="text/javascript">
	function GetXmlHttpObject()
    {
        if(window.XMLHttpRequest)
        {
           return new XMLHttpRequest();
        }
        else if(window.ActiveXObject)
        {
           return new ActiveXObject("Microsoft.XMLHTTP");
        }
        else
		{
		    return null;
        }
	}
	
	
    var xmlhttp;
    function subcategory()
    {
        if(xmlhttp.readyState==4)
        {
            var mytool_array=xmlhttp.responseText.split("<break>");
            for(i=0;i<(mytool_array.length)-1;i=i+2)
			{
                var opt = document.createElement("OPTION");
				opt.text = mytool_array[i];
				opt.value = mytool_array[i+1];
                document.product.subcategory.options.add(opt);
            }
        }
    }
	
	function choosecategory()
    {
        if(document.product.category.value=="select")
		{
		    alert("Choose category");
			document.product.category.focus();
			
			var total = document.product.subcategory.options.length;
            for(i=1;i<total;i=i+1)
            {
                document.product.subcategory.remove(1);
            }
			return false;
		}
		
		var total = document.product.subcategory.options.length;
        for(i=1;i<total;i=i+1)
        {
           document.product.subcategory.remove(1);
        }

        var category=document.product.category.value;
        xmlhttp=GetXmlHttpObject();
        if(xmlhttp==null)
        {
           alert ("Your browser does not support XML-HTTP!");
           return;
        }
        
		var url="ajax-subcategory.php";
        url=url+"?id="+category;
        xmlhttp.onreadystatechange=subcategory;
        xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
    }
	
	var xmlcheck;
    function city()
    {
        if(xmlcheck.readyState==4)
        {
            var mytool_array=xmlcheck.responseText.split("<break>");
            for(i=0;i<(mytool_array.length)-1;i=i+2)
			{
                var opt = document.createElement("OPTION");
				opt.text = mytool_array[i];
				opt.value = mytool_array[i+1];
                document.product.city.options.add(opt);
            }
        }
    }
	
	function choosecountry()
    {
        if(document.product.country.value=="select")
		{
		    alert("Choose country");
			document.product.country.focus();
			
			var total = document.product.city.options.length;
            for(i=1;i<total;i=i+1)
            {
                document.product.city.remove(1);
            }
			return false;
		}
		
		var total = document.product.city.options.length;
        for(i=1;i<total;i=i+1)
        {
           document.product.city.remove(1);
        }

        var country=document.product.country.value;
        xmlcheck=GetXmlHttpObject();
        if(xmlcheck==null)
        {
           alert ("Your browser does not support XML-HTTP!");
           return;
        }
        
		var url="ajax-city.php";
        url=url+"?id="+country;
        xmlcheck.onreadystatechange=city;
        xmlcheck.open("GET",url,true);
        xmlcheck.send(null);
    }
</script>


</head>

<body>

     <?PHP include('includes/admin-navbar.php') ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">

                <h4>
                    <?php
                    echo "Directory" . " - ";
                    if (!isset($_GET['id']) && empty($_GET['id'])) {
                        echo "Add New";
                    } else {
                        echo "Modify";
                    }
                    ?>
                </h4><br><br>


                        <div id="admSndClnAdd">

<?php
if (isset($_GET['id'])) {

    if (isset($_GET['message'])) {
        echo "<tr><td><b><font color='green'>" . $_GET['message'] . "</font></b></td></tr>";
    } else if (isset($_GET['errormessage'])) {
        echo "<tr><td><b><font color='red'>" . $_GET['errormessage'] . "</font></b></td></tr>";
    }

    echo "<div class='container'>";
    echo "<div class='row'>";
    echo "<div class='col-md-3'><a href='profile-audio.php?id=$directoryfield' class='btn btn-info' role='button'>Audio Management</a></div>";
    echo "<div class='col-md-3'><a href='profile-video.php?id=$directoryfield' class='btn btn-info' role='button'>Video Management</a></div>";
    echo "<div class='col-md-3'><a href='profile-image.php?id=$directoryfield' class='btn btn-info' role='button'>Image Management</a></div>";
    echo "</div>";
    echo "</div><br>";
}
?>

                            <form name="product" action="<?PHP $PHP_SELF ?>" method="post" onsubmit="return check_sheet()" >

                            <?PHP
                            if ($errormessage != "") {
                                echo "<tr><td colspan='2'>" . $errormessage . "</td></tr>";
                            }
                            ?>

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="">Category*</label>
                                            <td> <select name="category" onchange="choosecategory()" class="form-control">
                                                    <option value="select">select</option>
<?php
if (isset($_GET['id'])) {
    $query = "select categoryid from fyc_subcategory where subcategoryid='$subcategoryidfield'";
    $result = mysql_query($query);
    $data = mysql_fetch_assoc($result);
    $subcategoryid = $data['categoryid'];
    $query = "select categoryid, category from fyc_category where categorystatus=1 order by category";
} else {
    $query = "select categoryid, category from fyc_category where categorystatus=1 order by category";
}

$result = mysql_query($query);
while ($row = mysql_fetch_array($result)) {
    if (isset($_GET['sid']) && $row['categoryid'] == $categorydata['categoryid']) {
        ?>
                                                            <option value="<?php echo $row['categoryid']; ?>" selected><?php echo $row['category']; ?></option>
                                                            <?php
                                                        } else if (isset($_GET['id']) && $row['categoryid'] == $data['categoryid']) {
                                                            ?>
                                                            <option value="<?php echo $row['categoryid']; ?>" selected><?php echo $row['category']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $row['categoryid']; ?>"><?php echo $row['category']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td></div></div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="">Sub Category*</label>
                                            <td><select name="subcategory" class="form-control">
                                                    <option value="select">select</option>
                                                    <?php
                                                    if (isset($_GET['id'])) {
                                                        $query = "select subcategoryid, subcategory from fyc_subcategory where categoryid='$subcategoryid' order by subcategory asc";
                                                        $result = mysql_query($query);

                                                        while ($data = mysql_fetch_array($result)) {
                                                            if ($subcategoryidfield == $data['subcategoryid']) {
                                                                ?>
                                                                <option value="<?php echo $data['subcategoryid']; ?>" selected><?php echo $data['subcategory']; ?></option>
            <?php
        } else {
            ?>
                                                                <option value="<?php echo $data['subcategoryid']; ?>"><?php echo $data['subcategory']; ?></option>
            <?php
        }
    }
}

if (isset($_GET['sid'])) {
    $query = "select subcategoryid, subcategory from fyc_subcategory where categoryid='$categorydata[categoryid]' order by subcategory";
    $result = mysql_query($query);

    while ($data = mysql_fetch_array($result)) {
        if ($data['subcategoryid'] == $categorydata['subcategoryid']) {
            ?>
                                                                <option value="<?php echo $data['subcategoryid']; ?>" selected><?php echo $data['subcategory']; ?></option>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <option value="<?php echo $data['subcategoryid']; ?>"><?php echo $data['subcategory']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td></div></div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="">Cost</label>
                                            <td>
                                                <select name="cost" class="form-control">
                                                    <?php
                                                    $query = "select cost, costpoint from fyc_cost";
                                                    $result = mysql_query($query);
                                                    while ($data = mysql_fetch_array($result)) {
                                                        if (isset($_GET['id']) && $cost == $data['costpoint']) {
                                                            ?>
                                                            <option value="<?php echo $data['costpoint']; ?>" selected><?php echo $data['cost']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $data['costpoint']; ?>" ><?php echo $data['cost']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>	
                                                </select>

                                            </td>

                                        </div></div>
                                </div>



                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Title*</label>
                                            <td><input type="text" name="title" value="<?php echo $titlefield; ?>"  class="form-control" /></td>
                                        </div></div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Status*</label>
                                            <td>
                                                <select  name="status" class="form-control">
                                                    <option <?PHP if ($statusfield == "" || $statusfield == "1") {
                                                        echo "selected='selected'";
                                                    } ?> value="1">Active</option>
                                                    <option <?PHP if ($statusfield == "0") {
                                                        echo "selected='selected'";
                                                    } ?> value="0">InActive</option>
                                                </select>
                                            </td>

                                        </div></div></div>


                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <td><textarea name="description" class="form-control"><?php echo $about; ?></textarea></td>
                                        </div></div>

                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Address*</label>
                                            <td><textarea name="address" class="form-control"><?php echo $addressfield; ?></textarea></td>
                                        </div></div></div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Landmark</label>
                                            <td><input type="text" name="landmark" value="<?php echo $landmarkfield; ?>" class="form-control" /></td>
                                        </div></div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Zipcode</label>
                                            <td><input type="text" name="zipcode" value="<?php echo $zipcodefield; ?>" class="form-control" /></td>
                                        </div></div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">Country</label>

                                            <td>
                                                <select name="country" onchange="choosecountry()" class="form-control">
                                                    <option value="select">select</option>
<?php
if (isset($_GET['id'])) {
    $query = "select countryid from fyc_city where cityid='$cityfield'";
    $result = mysql_query($query);
    $data = mysql_fetch_assoc($result);
    $countryidid = $data['countryid'];
    $query = "select countryid, country from fyc_country order by country asc";
} else {
    $query = "select countryid, country from fyc_country order by country asc";
}


$result = mysql_query($query);
while ($row = mysql_fetch_array($result)) {

    if (isset($_GET['id']) && $row['countryid'] == $data['countryid']) {
        ?>
                                                            <option value="<?php echo $row['countryid']; ?>" selected><?php echo $row['country']; ?></option>
        <?php
    } else {
        ?>
                                                            <option value="<?php echo $row['countryid']; ?>" selected><?php echo $row['country']; ?></option>
        <?php
    }
}
?>
                                                </select>
                                            </td></div></div>

                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="">City</label>

                                            <td>
                                                <select name="city" class="form-control">
                                                    <option value="select">select</option>
                                                    <?php
                                                    // if(isset($_GET['id']))
                                                    // {
                                                    $query = "select cityid, city from fyc_city where countryid=2 order by city asc";
                                                    $result = mysql_query($query);

                                                    while ($data = mysql_fetch_array($result)) {
                                                        if ($cityfield == $data['cityid']) {
                                                            ?>
                                                            <option value="<?php echo $data['cityid']; ?>" selected><?php echo $data['city']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $data['cityid']; ?>"  
                                                            <?php
                                                            if ($data['cityid'] == ($_SESSION[$loginid] - 1)) {
                                                                echo "selected";
                                                            }
                                                            ?>
                                                                    ><?php echo $data['city']; ?></option>
                                                            <?php
                                                        }
                                                    }


                                                    // }
                                                    ?>
                                                </select>
                                            </td></div></div>

                                </div>


                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Url</label>
                                            <td><input type="text" name="url" value="<?php echo $urlfield; ?>" class="form-control"/></td>
                                        </div></div>
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Email</label>
                                            <td><input type="text" name="email" value="<?php echo $emailfield; ?>" class="form-control"/></td>
                                        </div></div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Contact Name</label>
                                            <td><input type="text" name="contactname" value="<?php echo $namefield; ?>" class="form-control"/></td>
                                        </div></div>
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Contact Number</label>
                                            <td><input type="text" name="contactnumber" value="<?php echo $numberfield; ?>" class="form-control"/></td>
                                        </div></div>
                                </div>	

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Verify Label</label>
                                            <td><input type="checkbox" name="verify_label" value="1" <?php
                                                if ($verify_label == '1') {
                                                    echo "checked";
                                                }
                                                ?>/></td>
                                        </div></div>
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Premium Label</label>
                                            <td><input type="checkbox" name="premium_label" value="1" <?php
                                                if ($premium_label == '1') {
                                                    echo "checked";
                                                }
                                                ?>/></td>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group"><label for="">Is Supplier</label>
                                            <td><input type="checkbox" name="issupplier" value="1" <?php
                                                if ($issupplier == '1') {
                                                    echo "checked";
                                                }
                                                ?>/></td>
                                        </div>
                                    </div>
                                </div>			            
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="submit"  name="btnSubmit" value="Submit" class="btn btn-success" />
                                        <input type="reset"   name="btnReset" value="Reset" class="btn btn-danger">
                                            <input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
                                    </td>
                                </tr>

                            </form>		


                        </div>
                        <!-- sndCln End -->
                        </div>
                        </div>
                        </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>



