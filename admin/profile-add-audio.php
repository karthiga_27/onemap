<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	if(isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
	    $_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
	    {
	        $query="select directoryid from fyc_directory where directoryid='$_GET[id]'";
	        $equery = mysql_query($query) or die(mysql_error());
	        if(mysql_num_rows($equery)<1)
	        {
	            header("location: profile-view.php?errormessage=Invalid profile to upload audio");
		        exit();
	        }
	    }
	    else
	    {
	        header("location: profile-view.php?errormessage=Invalid profile to upload audio");
		    exit();
	    }
    }
	else
	{
	    header("location: profile-view.php?errormessage=Invalid profile to upload audio");
		exit();
	}
	
	if(isset($_FILES))
	{
        if ($_FILES["audio"]["error"] > 0)
        {
            header("location: profile.php?id=$_GET[id]&errormessage=Zero Audio upload");
        }
        else
        {
		    $ext=pathinfo(strtolower(basename($_FILES["audio"]["name"])),PATHINFO_EXTENSION);
			$ext=strtolower($ext);
			$allowedExtensions = array("mp3");
			$j=count($allowedExtensions);
			
			for($i=0;$i<$j;$i++)
			{
			    if($allowedExtensions[$i]==$ext)
				{
				    break;
				}
			}
			
			if($i==$j)
			{
			    header("location: profile-audio.php?id=$_GET[id]&errormessage=Audio file type not support");
				exit();
			}
			else
			{
			    
				if($_FILES["audio"]["size"]<=5264518)
				{
				
                    $path = "../upload";
	                if(is_dir($path) == false){ mkdir($path, 0777, true); }
	
	                $path .= "/directory".$_GET['id'];
	                if(is_dir($path) == false){ mkdir($path, 0777, true); }
			
			        $path .= "/audio";
	                if(is_dir($path) == false){ mkdir($path, 0777, true); }
	            	    
			        $path=$path."/".$_FILES["audio"]["name"];
			        if(!move_uploaded_file($_FILES["audio"]["tmp_name"], $path))
			        {
			            header("location: profile-audio.php?errormessage=upload failed");
				        exit();
			        }
			        else
			        {
			    
				        $query="select directoryaudio from fyc_directory where directoryid='$_GET[id]'";
		                $equery = mysql_query($query) or die(mysql_error());
			            $data=mysql_fetch_assoc($equery);
			
			            if($data['directoryaudio']!="")
				        {
				            $path="../upload/directory";
			                $path=$path."$_GET[id]"."/audio/".$data['directoryaudio'];
				            if(is_file($path))
			                {
						        unlink($path);
			                }
				        }
				
				        $query="update fyc_directory set directoryaudio='".$_FILES["audio"]["name"]."'  where directoryid='$_GET[id]'";
				        $equery = mysql_query($query) or die(mysql_error());
				
				        header("location: profile-audio.php?id=$_GET[id]&message=Successfully audio upload");
				        exit();
				
			        }                 
				}
                else
                {
                    header("location: profile-audio.php?id=$_GET[id]&errormessage=Audio file size is big");
				    exit(); 
				}				
			}
		}
	}	
	
	
?>