<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	$titlefield="";
	$descriptionfield="";
	$timingfield="";
	$locationfield="";
	$statusfield="";
	
	if(isset($_GET['id']) && $errormessage=="")
	{
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query = "select eventtitle, eventdescription, eventtiming, eventlocation, eventstatus from fyc_event where eventid = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
	        
	        if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
				$titlefield=$fetchrow['eventtitle'];
		        $descriptionfield=$fetchrow['eventdescription'];
				$timingfield=$fetchrow['eventtiming'];
				$locationfield=$fetchrow['eventlocation'];
		        $statusfield=$fetchrow['eventstatus'];
		    }
		    else
	 	    {
		        header("location: events-view.php?errormessage=Invalid event");
		    }
        }
		else
	    {
		    header("location: events-view.php?errormessage=Invalid event");
		}
	}
			
		
	if(isset($_POST['btnSubmit']) == true) 
    {
	    $_POST['eventtitle']=mysql_escape_string(htmlentities($_POST['eventtitle'])); 
		$_POST['eventdescription']=mysql_escape_string(htmlentities($_POST['eventdescription'])); 
		$_POST['eventtiming']=mysql_escape_string(htmlentities($_POST['eventtiming'])); 
		$_POST['eventlocation']=mysql_escape_string(htmlentities($_POST['eventlocation'])); 
		$_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);
				
		if($_POST['eventtitle']!="" && $_POST['eventdescription']!="" && $_POST['eventtiming']!="" && $_POST['eventlocation']!="" && ($_POST['status']==1 || $_POST['status']==0))
        {
                if(isset($_GET['id']))
			    {
			        $query = "update fyc_event set eventtitle = '$_POST[eventtitle]', eventdescription = '$_POST[eventdescription]',  eventtiming = '$_POST[eventtiming]', eventlocation = '$_POST[eventlocation]', eventstatus = '$_POST[status]', updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp() where  eventid = '$_GET[id]'";
			    }
			    else
			    {
			        $query = "insert into fyc_event (eventtitle, eventdescription, eventtiming, eventlocation, eventstatus, createdby, createddatetime) values ('$_POST[eventtitle]','$_POST[eventdescription]','$_POST[eventtiming]','$_POST[eventlocation]','$_POST[status]','$_SESSION[$loginid]',current_timestamp())";
			    } 
			
			    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		        if($result == true) 
		        {
			         //--------------------------------------------------
					if(basename($_FILES["image"]["name"])!="")
					{
					if ($_FILES["image"]["error"] > 0)
                    {
                        $errormessage=$_FILES["image"]["name"]." upload failed";
						if(isset($_GET['forward']))
						{ header("location: events-view.php?errormessage=$errormessage&forward=$_GET[forward]"); }
						else
						{ header("location: events-view.php?errormessage=$errormessage&forward=0"); }
						exit();
			        }
			        else
					{
					    $ext=pathinfo(basename($_FILES["image"]["name"]),PATHINFO_EXTENSION);
						$ext=strtolower($ext);
						$allowedExtensions = array("jpeg","jpg","gif");
			            $n=count($allowedExtensions);
			
			            for($m=0;$m<$n;$m++)
			            {
			                if($allowedExtensions[$m]==$ext)
				            {
				            break;
				            }
			            }
						
						if($m==$n)
						{
						    $errormessage=$_FILES["image"]["name"]." upload failed";
						    if(isset($_GET['forward']))
						    { header("location: events-view.php?id=errormessage=$errormessage&forward=$_GET[forward]"); }
						    else
						    { header("location: events-view.php?errormessage=$errormessage&forward=0"); }
						    exit();
					    }
						else
						{
						    $path="../events";
					        if(is_dir($path) == false){ mkdir($path, 0777, true); }
					        
							$original = $path."/original";
	                        if(is_dir($original) == false){ mkdir($original, 0777, true); }
							
							$medium = $path."/medium";
	                        if(is_dir($medium) == false){ mkdir($medium, 0777, true); }
							
							$thumbnail = $path."/thumbnail";
	                        if(is_dir($thumbnail) == false){ mkdir($thumbnail, 0777, true); }
							
						
							if(isset($_GET['id'])) 
							{
							    $imagename='i'.$_GET['id'].".".$ext;
							}
							else
							{
							    $imagename='i'.(mysql_insert_id()).".".$ext;
							}
							
					        
			                $original=$original."/".$imagename;		
							
							if(move_uploaded_file($_FILES["image"]["tmp_name"], $original)==true)
			                {
							    
								/**************************** Thumb Size **********************************/
								$medium=$medium."/".$imagename;
                                $width=270;
						        $height=233;
						        thumbnail($original,$medium,$width,$height);
								/***************************************************************/
								
								/**************************** Medium Size **********************************/
								$thumbnail=$thumbnail."/".$imagename;
                                $width=50;
						        $height=50;
						        thumbnail($original,$thumbnail,$width,$height);
						        /***************************************************************/
							    
								
								if(isset($_GET['id'])) 
								{
							    $querya="update fyc_event set eventimage='events/original/$imagename', mediumimage='events/medium/$imagename' ,thumbimage='events/thumbnail/$imagename' where eventid='$_GET[id]'";
		                        }
								else
								{
								$pop=mysql_insert_id();
							    $querya="update fyc_event set eventimage='events/original/$imagename', mediumimage='events/medium/$imagename' ,thumbimage='events/thumbnail/$imagename' where eventid='$pop'";
		                        }
								$equery = mysql_query($querya) or die(mysql_error());
								
							}
							else
							{
							    $errormessage=$_FILES["image"]["name"]." upload failed";
						        if(isset($_GET['forward']))
						        { header("location: events-view.php?errormessage=$errormessage&forward=$_GET[forward]"); }
						        else
						        { header("location: events-view.php?errormessage=$errormessage&forward=0"); }
						        exit();
							}
						}
					}
					}
					//--------------------------------------------------
					
					if(isset($_GET['id'])) 
			        {
				        header("location: events-view.php?message=update&forward=$_GET[forward]");
						exit();
			        }
			        else 
			        {
				        header("location: events-view.php?message=save&forward=$_GET[forward]");
						exit();
			        }
		        }		
		        else
				{
				    header("location: events-view.php?errormessage=Fail to add or update event&forward=$_GET[forward]");
					exit();
				}
		}
        else
        {
        $errormessage="Data incomplete";
        }		
	}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function FormValidation(DocumentForm) 
	{
		
		if(DocumentForm.eventtitle.value == "") 
		{
			alert("Enter Event Title");
			DocumentForm.eventtitle.focus();
			return false;
		}
		
		if(DocumentForm.eventdescription.value == "") 
		{
			alert("Enter Description");
			DocumentForm.description.focus();
			return false;
		}
		
		if(DocumentForm.eventtiming.value == "") 
		{
			alert("Enter Timing");
			DocumentForm.timing.focus();
			return false;
		}
		
		if(DocumentForm.eventlocation.value == "") 
		{
			alert("Enter Location");
			DocumentForm.location.focus();
			return false;
		}
		
		return true;
	}
	
	function Cancel_ClickEvent() 
	{
		window.location.href = "events-view.php";
	}
</script>
</head>

<body>

     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>
		<?php 
	        
			echo "Event"." - ";
	        if(!isset($_GET['id'])) 
	        { echo "Add New"; } 
	        else 
	        { echo "Modify";  }
        ?>
		</h4><br><br>
		<div id="admSndClnAdd">
		
		<form name="dataform" action="<?PHP $PHP_SELF ?>" method="post" onSubmit="return FormValidation(this);" ENCTYPE="multipart/form-data">

		
		<?PHP
		if ($errormessage != "") { echo "<tr><td colspan='2'><h4>".$errormessage."</h4></td></tr>"; }
	    ?>
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Title</label>
		<td><input type="text"  name="eventtitle" value="<?PHP echo $titlefield; ?>" class="form-control" /></td>
	    </div></div></div>
	    
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Description</label>
		<td><textarea name="eventdescription" class="form-control" rows="5" cols="20"><?PHP echo $descriptionfield; ?></textarea></td>
	    </div></div></div>
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Timing</label>
		<td><textarea name="eventtiming" rows="5" cols="20" class="form-control"><?PHP echo $timingfield; ?></textarea></td>
	    </div></div></div>
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Location</label>
		<td><textarea name="eventlocation" rows="5" cols="20" class="form-control"><?PHP echo $locationfield; ?></textarea></td>
	     </div></div></div>
		
		
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Image</label>
		            <td><input type="file"  name="image" class="form-control"/></td>
	               </div></div>
					<div class="col-lg-5">
					<div class="form-group"><label for="">Status</label>
		<td>
			<select  name="status" class="form-control">
				<option <?PHP if($statusfield == "" || $statusfield == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($statusfield == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	    </div></div></div>
	
	    <tr>
		<td></td>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		
		</form>
		</div>
       	
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>






<?php

    function thumbnail($source,$destination,$width,$height)
	{	   
		   $sizes = getimagesize($source);
			
			$expected_max_width		=   $height;
            $expected_max_height	=   $height;
			
			
            $originalw	=	$sizes[0];
            $originalh	=	$sizes[1];
    
            if ($originalh<$expected_max_height) 
			{
                if ($originalw<$expected_max_width)
				{
                    $imgwidth	=	$originalw;
                    $imgheight	=	$originalh;
                } 
				else 
				{
                    $imgheight	=	($expected_max_width * $originalh)/ $originalw;
                    $imgwidth	=	$expected_max_width;
                }
            } 
			else 
			{
                $new_height		=	$expected_max_height;
                $new_width		=	($expected_max_height * $originalw)/ $originalh;
    
                if ($new_width>$expected_max_width) 
				{
                    $new_height	=	($expected_max_width * $expected_max_height) / $new_width;
                    $new_width	=	$expected_max_width;
                }
    
                $imgwidth	=	$new_width;
                $imgheight	=	$new_height;
            }
            $new_h	=	$imgheight;
            $new_w	=	$imgwidth;
			$dest = imagecreatetruecolor($new_w,$new_h);


            $im=ImageCreateFromJPEG($source); 
            $width=ImageSx($im);              
            $height=ImageSy($im);     

            $imageResized=imagecreatetruecolor($new_w,$new_h);
            $imageTmp=imagecreatefromjpeg($source);
            imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0,$new_w,$new_h, $width, $height);
            ImageJPEG($imageResized,$destination);
			//unlink($temp);
	}
?>

