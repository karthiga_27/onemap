<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$pagename = "user";
	$redirecturl = "user-view.php";
	
	$errormessage="";

	if (isset($_POST['cancel']) == true) { header("location: $redirecturl"); }
	
	if (isset($_POST['submit']) == true) {
		$firstname=$fetchrow['firstname'];
		$lastname=$fetchrow['lastname'];
		$photo=$fetchrow['city'];
		$loginid=$fetchrow['photo'];
		$password=$fetchrow['password'];
		$email=$fetchrow['email'];
		$telephone=$fetchrow['telephone'];
		$address=$fetchrow['address'];
		$dob=$fetchrow['dob'];
		$newsletter=$fetchrow['newsletter'];
		$memberstatus=$fetchrow['memberstatus'];
		
		if(isset($_GET['memberid']) && $_GET['memberid'] != "") 
		{
			$query = "update fyc_member set firstname = '$firstname', lastname = '$lastname', photo='$photo', password='$password', email='$email', telephone='$telephone', address='$address', memberstatus = '$memberstatus'where (memberid = ".$_GET['memberid'].")";
		}	
		else 
		{
			$query = "insert into fyc_member (firstname, lastname, photo, password, email, telephone, address, memberstatus ) values ('$firstname', '$lastname', '$photo', '$password', '$email', '$telephone', '$address', '$memberstatus')";
		}
		
	    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		if ($result == true) {
			
				header("location: $redirecturl?mode=save".$countryurl);
			}
			else {
				header("location: $redirecturl?mode=update".$countryurl);
			}
		}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?PHP echo $projecttitle." :: ".$pagename; ?></title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/JScripts.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	function FormValidation(DocumentForm) {
		DocumentForm.city.value = trim(DocumentForm.city);

		if(DocumentForm.city.value == "") {
			alert("Enter City");
			DocumentForm.city.focus();
			return false;
		}
		
		return true;
	}
	
	function Cancel_ClickEvent() {
		window.location.href = "<?php echo $redirecturl; ?>";
	}
</script>
</head>

<body onload="javascript:document.getElementById('city').focus();">

<!-- body Begin -->
<div id="body">

<div id="bodyBg">

<?php include('includes/admin-header.php') ?>

<!-- content Begin -->
<div id="content">

<!-- fstCln Begin -->
<div id="fstCln">

<?PHP include('includes/admin-quicklinks.php') ?>

</div>
<!-- fstCln End -->

<div></div>

<!-- sndCln Begin -->
<div id="admSndCln">

<h1>
<?php 
	echo $pagename." - ";
	if(isset($_GET['id']) && $_GET['id'] == "") { echo "Add New"; } else { echo "Modify"; }
?>
</h1>

<div id="admSndClnAdd">

<?PHP

	$memberid="";
	$firstname="";
	$lastname="";
	$photo="";
	$loginid="";
	$password="";
	$email="";
	$telephone="";
	$address="";
	$dob="";
	$newsletter="";
	$memberstatus="";
	
	if(isset($_GET['memberid']) && $_GET['memberid'] != "" && $errormessage == "") {
		$query = "select memberid, firstname, lastname, photo, memberstatus, loginid, password, email, telephone, address, dob, newsletter from fyc_member where (memberid = ".$_GET['memberid'].")";
		$equery = mysql_query($query) or die(mysql_error());
		$fetchrow = mysql_fetch_array($equery);
		$firstname=$fetchrow['firstname'];
		$lastname=$fetchrow['lastname'];
		$loginid=$fetchrow['photo'];
		$password=$fetchrow['password'];
		$email=$fetchrow['email'];
		$telephone=$fetchrow['telephone'];
		$address=$fetchrow['address'];
		$dob=$fetchrow['dob'];
		$newsletter=$fetchrow['newsletter'];
		$memberstatus=$fetchrow['memberstatus'];
	}
?>
<form id="Form1" name="Form1" action="<?PHP $PHP_SELF ?>" method="post" onSubmit="return FormValidation(this);">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<?php
		if ($errormessage != "") { echo "<tr><td colspan='2'>".$errormessage."</td></tr>"; }
	?>
	<tr>
		<td>First Name</td>
		<td><input type="text" id="firstname" name="firstname" value="<?PHP echo $firstname; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>Last Name</td>
		<td><input type="text" id="lastname" name="lastname" value="<?PHP echo $lastname; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>Photo</td>
		<td><input type="text" id="photo" name="photo" value="<?PHP echo $photo; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>Password</td>
		<td><input type="text" id="password" name="password" value="<?PHP echo $password; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>email</td>
		<td><input type="text" id="email" name="email" value="<?PHP echo $email; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>telephone</td>
		<td><input type="text" id="telephone" name="telephone" value="<?PHP echo $telephone; ?>" class="input" /></td>
	</tr>
	<tr>
		<td>Address</td>
		<td><textarea name="address" id="address" rows="4" cols="25"><?php echo $address; ?></textarea></td>
		</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select id="memberstatus" name="memberstatus" class="select">
				<option <?PHP if($memberstatus == "" || $memberstatus == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($memberstatus == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" id="submit" name="submit" value="Submit" class="button" />
			<input type="reset" id="reset" name="reset" value="Reset" class="button">
			<input type="button" id="cancel" name="cancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="button" />
		</td>
	</tr>
</table>

</div>

</form>
</div>
<!-- sndCln End -->

</div>
<!-- content End -->

<div></div>

<?PHP include('includes/admin-footer.php') ?>

</div>

</div>
<!-- body End -->

</body>
</html>