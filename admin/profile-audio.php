<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	if(isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query="select id from fyc_directory where id='$_GET[id]'";
	        $equery = mysql_query($query) or die(mysql_error());
   	        if(mysql_num_rows($equery)==0)
	        {
	            header("location: profile-view.php?errormessage=Invalid profile to upload audio");
				exit();
	        }
		}
		else
		{
		    header("location: profile-view.php?errormessage=Invalid profile to upload audio");
			exit();
		}
	}
	else
	{
	    header("location: profile-view.php?errormessage=Profile not found");
		exit();
	}
	
	if(isset($_GET['message']))
	{
	    $message=$_GET['message'];
	}
	
	if(isset($_GET['errormessage']))
	{
	    $errormessage=$_GET['errormessage'];
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function Cancel_ClickEvent() 
	{
		window.location.href = "profile.php?id=<?php echo $_GET['id']; ?>";
	}
	
	function Video_ClickEvent(next) 
	{
	   window.location.href = "profile-video.php?id="+next;
	}
	
	function Image_ClickEvent(next) 
	{
	   window.location.href = "profile-image.php?id="+next;
	}
	
</script>
</head>

<body>


     <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
		
		<h4>
		<?php 
	        
		    echo "Audio Gallery";
	    
		?>
		</h4>
		<tr>
		
		<td>
		<form name="dataform" action="profile-add-audio.php?id=<?php echo $_GET['id']; ?>" method="post" ENCTYPE="multipart/form-data">
	
		
		
		<?php
		if($message != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		?>
	
		
		<div class="row">
					<div class="col-lg-5">
					<div class="form-group"><label for="">Audio</label>
						<input type="file" name="audio" class="form-control">
					</div></div>
	    </div>
	    
		<tr>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
			<?php
			echo "<input type='button'  name='btnCancel' value='go to video' onclick='javascript:Video_ClickEvent($_GET[id]);' class='btn btn-primary' />";
		    echo "<input type='button'  name='btnCancel' value='go to Image' onclick='javascript:Image_ClickEvent($_GET[id]);' class='btn btn-info' />";
			?>
		</td>
	    </tr>
		
		</form>
		</td>
		                           
		<td>
		<?php
		$query = "select directoryaudio from fyc_directory where directoryid='$_GET[id]'";
		$equery = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($equery)>=1)
		{
		    $fetch=mysql_fetch_assoc($equery);
		    if($fetch['directoryaudio'] != "")
		    {   
			    $path="../upload/directory";
				$path=$path."$_GET[id]"."/audio/".$fetch['directoryaudio'];  
				?>
				<object classid='clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95' width='348' height='308'>
				<param name='play' value='false' >
				<param name='controller' value='true' />
                <param name='autoplay' value='false' />
                <param name='autostart' value='0' /> 
                <param name='src' value='<?php echo $path; ?>' />
				<embed type='application/x-mplayer2' width='200' height='90' src='<?php echo $path; ?>' alt='audio' title='audio' autostart='0' showcontrols=1 />
                </object> 
				<br/>
				<br/>
				<?php
				echo "<a href='profile-delete-audio.php?id=$_GET[id]&del=$_GET[id]'>Delete this audio</a>";
		    }
		}
		?>
		</td>
		
		</tr>
        
						
		
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

