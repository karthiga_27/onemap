<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	
	$categoryid="";
	$subcategoryfield="";
	$statusfield="";
	$iconfield = "";
	
	if(isset($_GET['id']) && $errormessage=="")
	{
		
		$_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
		{
		    $query = "select subcategory, icon, subcategorystatus, categoryid from fyc_subcategory where subcategoryid = '$_GET[id]'";
		    $equery = mysql_query($query) or die(mysql_error());
	        
		    if(mysql_num_rows($equery)>=1)
		    {
		        $fetchrow = mysql_fetch_assoc($equery);
			    $categoryid=$fetchrow['categoryid'];
			    $subcategoryfield=$fetchrow['subcategory'];
		        $statusfield=$fetchrow['subcategorystatus'];
		        $iconfield = $fetchrow['icon'];
			}
	  	    else
	 	    {
		        header("location: subcategory-view.php?errormessage=Invalid subcategory");
		    }
		}
		else
	    {
		    header("location: subcategory-view.php?errormessage=Invalid subcategory");
		}
    }
			
	
	
	
	if(isset($_POST['btnSubmit']) == true) 
	{
	    if($_POST['category']!="select" && $_POST['subcategory']!="" && ($_POST['status']==1 || $_POST['status']==0))
        {
                $_POST['category']=mysql_escape_string(htmlentities($_POST['category'])); 
		        $_POST['subcategory']=mysql_escape_string(htmlentities($_POST['subcategory'])); 
			    $_POST['status']=filter_var($_POST['status'], FILTER_VALIDATE_INT);
			    $_POST['icon']=mysql_escape_string(htmlentities($_POST['icon'])); 
			
			    if(isset($_GET['id']))
			    {
			        $query = "update fyc_subcategory set icon='$_POST[icon]', subcategory = '$_POST[subcategory]', subcategorystatus = '$_POST[status]', categoryid='$_POST[category]' ,updatedby = '$_SESSION[$loginid]', updateddatetime = current_timestamp() where subcategoryid = '$_GET[id]'";
			    }
			    else
			    {
			        $query = "insert into fyc_subcategory (icon, subcategory, subcategorystatus,  categoryid, createdby, createddatetime) values ('$_POST[icon]', '$_POST[subcategory]', '$_POST[status]', '$_POST[category]','$_SESSION[$loginid]', current_timestamp())";
			    }
			
			    $result = mysql_query($query) or ($errormessage = "<span class='errormessage'>".mysql_error()."</span>");
		        if($result == true) 
		        {
			        if(isset($_GET['id'])) 
			        {
				        header("location: subcategory-view.php?id=$_POST[category]&message=update&forward=$_GET[forward]");
			        }
			        else 
			        {
				        header("location: subcategory-view.php?id=$_POST[category]&message=save&forward=$_GET[forward]");
			        }
		        }		
		}
        else
        {
            $errormessage="Data incomplete";
        }		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <?PHP include('includes/admin-meta.php') ?>
<script language="javascript" type="text/javascript">
    function FormValidation(DocumentForm) 
	{
		if(DocumentForm.category.value == "select") 
		{
			alert("Select category");
			DocumentForm.category.focus();
			return false;
		}
		
		DocumentForm.subcategory.value = trim(DocumentForm.subcategory);
        if(DocumentForm.subcategory.value == "") 
		{
			alert("Enter subcategory");
			DocumentForm.subcategory.focus();
			return false;
		}
		
		return true;
	}
	
	function Cancel_ClickEvent() 
	{
		window.location.href = "subcategory-view.php";
	}
</script>
</head>

<body>

  <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>
		<?php 
	        
			echo "Subcategory"." - ";
	        if(!isset($_GET['id'])) 
	        { echo "Add New"; } 
	        else 
	        { echo "Modify";  }
        ?>
		</h4><br><br>
		<div id="admSndClnAdd">
		
		<form name="dataform" action="<?PHP $PHP_SELF ?>" method="post" onSubmit="return FormValidation(this);">
		
		
		<?PHP
		if ($errormessage != "") 
		{ echo "<tr><td colspan='2'><h4>".$errormessage."</h4></td></tr>"; }
	    ?>
		
		<div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Category</label>
		<select name="category" class="form-control">
		<option value="select">select</option>
		<?php
		$query="select categoryid, category from fyc_category where categorystatus=1";
		$equery = mysql_query($query) or die(mysql_error());
		while($fetchrow=mysql_fetch_array($equery))
		{
		    if(isset($_GET['id']) && $fetchrow['categoryid']==$categoryid && $errormessage=="")
			{
			?>
	        <option value="<?php  echo $fetchrow['categoryid']; ?>" selected><?php echo $fetchrow['category']; ?></option> 
	        <?php
			}
			else
			{
			?>
	        <option value="<?php  echo $fetchrow['categoryid']; ?>"><?php echo $fetchrow['category']; ?></option> 
	        <?php
			}
		}
		?>
		</select>
		</div></div></div>
		
		<div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Subcategory</label>
		<input type="text"  name="subcategory" value="<?PHP echo $subcategoryfield; ?>" class="form-control" /></td>
	    </div>
	    </div>
	    </div>
	    
	    <div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Font Icon</label>
		<input type="text"  name="icon" value="<?PHP echo $iconfield; ?>" class="form-control" /></td>
	    </div>
	    </div>
	    </div>
	    
		<div class="row">
	    <div class="col-lg-5">
	     <div class="form-group">
		<td><label for="">Status</label>
			<select  name="status" class="form-control">
				<option <?PHP if($statusfield == "" || $statusfield == "1") { echo "selected='selected'"; } ?> value="1">Active</option>
				<option <?PHP if($statusfield == "0") { echo "selected='selected'"; } ?> value="0">InActive</option>
			</select>
		</td>
	    </div></div></div>
	
	    <tr>
		<td></td>
		<td>
			<input type="submit"  name="btnSubmit" value="Submit"  class="btn btn-success" />
			<input type="reset"   name="btnReset" value="Reset"    class="btn btn-danger" />
			<input type="button"  name="btnCancel" value="Cancel" onclick="javascript:Cancel_ClickEvent();" class="btn btn-warning" />
		</td>
	    </tr>
		
		</form>
		
</div>
<!-- sndCln End -->
 </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>

