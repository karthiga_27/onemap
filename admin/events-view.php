<?php
	include("includes/config-variables-admin.php");
	include("includes/config.php");
	if($_SESSION[$loginid] == "") { header("location: login.php?access=denied");  }
	
	$message="";
	$errormessage="";
	if($_SESSION[$logincategory]==1 && isset($_GET['id']))
	{
	    $_GET['id']=mysql_escape_string(htmlentities($_GET['id'])); 
		$_GET['id']=filter_var($_GET['id'], FILTER_VALIDATE_INT);
	    if($_GET['id']>=1)
	    {
	         $query="select eventimage, mediumimage, thumbimage from fyc_event where eventid='$_GET[id]'";
			$equery = mysql_query($query) or die(mysql_error());	
			if(mysql_num_rows($equery)==1)
			{
			    $data=mysql_fetch_assoc($equery);
			    
			    $pathfound="../".$data['eventimage'];
				if(is_file($pathfound))
				{
				    unlink($pathfound);
			    }
				
				$pathfound="../".$data['mediumimage'];
				if(is_file($pathfound))
				{
				    unlink($pathfound);
			    }
				
				$pathfound="../".$data['thumbimage'];
				if(is_file($pathfound))
				{
				    unlink($pathfound);
			    }
				
				$Query = "delete from fyc_event where eventid = '$_GET[id]'";
		        $EQuery = mysql_query($Query) or die(mysql_error());
		        $message = "Record deleted successfully";
			}
			else
			{
			    $errormessage = "Invalid event";
		    }
	    }
		else
		{
		    $errormessage="Invalid event";
		}
	}
	
	if(isset($_GET['message'])) 
	{
		$_GET['message']=mysql_escape_string(htmlentities($_GET['message'])); 
		$message = "Record ".$_GET['message']."d successfully";
	}
	else if(isset($_GET['errormessage'])) 
	{
		$_GET['errormessage']=mysql_escape_string(htmlentities($_GET['errormessage'])); 
		$errormessage=$_GET['errormessage'];
	}
	
?>

<!DOCTYPE HTML>
<html>
</head>
    <?PHP include('includes/admin-meta.php') ?>
</head>
<body>
  <div id="wrapper">
         <?PHP include('includes/admin-navbar.php') ?>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            <div class="container">
        <h4>Event</h4>
		<?php
		
		$start=0;
		if(isset($_GET['forward']))
		{
		    $_GET['forward']=mysql_escape_string(htmlentities($_GET['forward'])); 
		    $_GET['forward']=filter_var($_GET['forward'], FILTER_VALIDATE_INT);
	        
			if($_GET['forward']>=1)
			{
			    $start=$_GET['forward'];
		    }
			else
			{
			    $start=0;
			}
		}
		
		$query = "select eventid, eventtitle, eventstatus from fyc_event order by eventid desc limit $start, 11 ";
	    $equery = mysql_query($query);
		
		echo "<div class='row'><div class='col-md-10 hidden-sm hidden-xs'>";
        echo "<a href='events.php' title='Click here to add new event' class='btn btn-info' role='button' style='float:right'>Add New</a>";
        echo "</div></div>";
		
		echo "<table>";
		
		if($message != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='green'>".$message."</font></b></td></tr>";
	    }
		else if($errormessage != "") 
	    {
		    echo "<tr><td colspan='7'><b><font color='red'>".$errormessage."</font></b></td></tr>";
	    }
		echo "<thead><tr><th>Sl No</th><th>Event</th><th>Status</th><th>E</th>";
		
		
		if($_SESSION[$logincategory]==1)
	    {
	        echo "<th>D</th></tr></thead>";
	    }
		
		if(mysql_num_rows($equery)==0) 
	    {
		    echo "<tr><td colspan='7' align='center'><font color='red'><b>There are no records</b></font></td</tr>";
	    }
		else
		{
		    $iRow = 1;
			while( $iRow<=10 && $fetchrow = mysql_fetch_array($equery))
            {
                echo "<tbody><tr onmouseover=\"this.className='onmouseovertr';\" onmouseout=\"this.className='onmouseouttr'\">";
			    echo "<td data-column='Sl No'>".$iRow."</td>";
			    echo "<td data-column='Event'>".$fetchrow['eventtitle']."</td>";
				
				echo "<td data-column='Status'>";
			    if($fetchrow['eventstatus'] == "1") { echo "Active"; } else { echo "InActive"; }
			    echo"</td>";

				echo "<td data-column='E'><a href='events.php?id=$fetchrow[eventid]&forward=$start'><img src='../images/edit.gif' alt='Edit' title='Edit'></a></td>";
            
			    if($_SESSION[$logincategory]==1)
	            {
			        echo "<td data-column='D'><a href='#' onclick=\"DeleteConfirmation('events-view.php?id=$fetchrow[eventid]&forward=$start','$fetchrow[eventtitle]')\"><img src='../images/delete.jpg' alt='Delete' title='Delete' /></a></td>";
		        }
			
			    echo "</tr></tbody>";
			    $iRow += 1;
			}	
        }
		echo "</table>";
		
	
		
			if($start>=10)
			{
			$previous=$start-10;
			echo "<a href='events-view.php?forward=$previous'><img src='../images/previous.png' /></a>";
			}
			echo "&nbsp;&nbsp;&nbsp;";
			if(mysql_num_rows($equery)==11)
			{
			$next=$start+10;
			echo "<a href='events-view.php?forward=$next'><img src='../images/forward.png' /></a>";
			}
       		
		
		
		?>
      </div>
      </div>
 </div>
 
     <?PHP include('includes/admin-footer.php') ?>
</body>
</html>
